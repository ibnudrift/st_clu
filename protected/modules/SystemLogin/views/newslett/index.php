<?php
$this->breadcrumbs=array(
	'Newsletter',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Newsletter',
	'subtitle'=>'Data Newsletter',
);

$this->menu=array(
	// array('label'=>'Add Newsletter', 'icon'=>'th-list','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>
<!-- <h1>Newsletter</h1> -->
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'tb-newsletter-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		'nama',
		'email',
		'date_input',
		// 'status',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
		),
	),
)); ?>
