<?php

class UnitMasterController extends ControllerAdmin
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layoutsAdmin/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			//'accessControl', // perform access control for CRUD operations
			array('SystemLogin.filter.AuthFilter'),
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			(!Yii::app()->user->isGuest)?
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete','index','view','create','update'),
				'users'=>array(Yii::app()->user->name),
			):array(),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new UnitMaster;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['UnitMaster']))
		{
			$model->attributes=$_POST['UnitMaster'];
			if($model->validate()){
				$transaction=$model->dbConnection->beginTransaction();
				try
				{
					$model->save();
					Log::createLog("UnitMasterController Create $model->id");
					Yii::app()->user->setFlash('success','Data has been inserted');
				    $transaction->commit();
					$this->redirect(array('index'));
				}
				catch(Exception $ce)
				{
				    $transaction->rollback();
				}
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['UnitMaster']))
		{
			$model->attributes=$_POST['UnitMaster'];
			if($model->validate()){
				$transaction=$model->dbConnection->beginTransaction();
				try
				{
					$model->save();
					Log::createLog("UnitMasterController Update $model->id");
					Yii::app()->user->setFlash('success','Data Edited');
				    $transaction->commit();
					$this->redirect(array('index'));
				}
				catch(Exception $ce)
				{
				    $transaction->rollback();
				}
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		// if(Yii::app()->request->isPostRequest)
		// {
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			// if(!isset($_GET['ajax']))
			// isset($_POST['returnUrl']) ? $_POST['returnUrl'] :
				$this->redirect( array('index'));
		// }
		// else
			// throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new UnitMaster('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['UnitMaster']))
			$model->attributes=$_GET['UnitMaster'];

		$dataProvider=new CActiveDataProvider('UnitMaster');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=UnitMaster::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='unit-master-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionCSV()
	{
		if ($_GET['file'] != '') {
			if (($handle = fopen(Yii::getPathOfAlias('webroot').'/images/csv/'.urldecode($_GET['file']), "r")) !== FALSE) {
				$row = 0;
				$dataCsv = array();
			    while (($data = fgetcsv($handle)) !== FALSE) {
			        $num = count($data);
			        for ($c=0; $c < $num; $c++) {
			        	$dataCsv[$row][$c] = $data[$c];
			        }
			        $row++;
			    }
			    fclose($handle);
			    unset($dataCsv[0]);
			}

			if (isset($_POST['submit'])) {
				
				foreach ($dataCsv as $key => $value) {
					// $dataUnit = UnitMaster::model()->find('ktp_no = :ktp_no', array(':ktp_no'=>$value[0]));
					// if ($dataUnit == null) {
						$dataUnit = new UnitMaster;
					// }
					$dataUnit->ktp_no = trim($value[0]);
					$dataUnit->blok = trim($value[1]);
					$dataUnit->kav = trim($value[2]);
					$dataUnit->tipe_rumah = trim($value[3]);
					$dataUnit->nama_pemilik = trim($value[4]);
					$dataUnit->phone = trim($value[5]);
					$dataUnit->bulan_st = trim($value[6]);
					// $dataUnit->jadwal_st = date('Y-m-d', strtotime(trim($value[6])));
					$dataUnit->bsc_marketing = trim($value[7]);
					$dataUnit->bsc_phone = trim($value[8]);
					$dataUnit->admin = trim($value[9]);
					$dataUnit->project = trim($value[10]);

					if ($value[0] != '') {
						$dataUnit->save(false);
					}

				    // $transaction->commit();
				}

				Yii::app()->user->setFlash('success','Data has been Imported!...');
				$this->redirect(array('csv'));
			}

			$this->render('csv_show',array(
				'model'=>$model,
				'dataCsv'=>$dataCsv,
			));
		}else{
			$model = new CsvForm;
			$model->scenario = 'csv';
			if($_POST['submit'] == 'submit')
			{
				$model->attributes = $_POST['CsvForm'];

				$file = CUploadedFile::getInstance($model,'file');
				$model->file = substr(md5(time()),0,5).'-'.$file->name;
				
				if($model->validate()){
					$file->saveAs(Yii::getPathOfAlias('webroot').'/images/csv/'.$model->file);

					$this->redirect(array('csv', 'file'=>urlencode($model->file)));

				}
			}

			$this->render('csv',array(
				'model'=>$model,
			));
		}
	}

}
