<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <meta name="keywords" content="<?php echo CHtml::encode($this->metaKey); ?>">
    <meta name="description" content="<?php echo CHtml::encode($this->metaDesc); ?>">
    
    <script src="<?php echo Yii::app()->baseUrl; ?>/asset/js/jquery.min.js"></script>

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/asset/vendor/bootstrap/css/bootstrap.min.css" />
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/asset/js/bootstrap-4.0.0/bootstrap.min.js"></script>

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/asset/css/landing-page.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/asset/css/styles.css">

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/asset/css/media.styles.css">

    <?php /*
    <link href="<?php echo Yii::app()->baseUrl; ?>/asset/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->baseUrl; ?>/asset/vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    */ ?>
    
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <?php echo $this->setting['google_tools_webmaster']; ?>
    <?php echo $this->setting['google_tools_analytic']; ?> 
    </head>
    <body data-spy="scroll" id="page-top">
        <div class="wrapper">
            <?php echo $content ?>
        </div>

    <script type="text/javascript">
        $(function(){
            $('.t-backtop, .back-to-top').click(function () {
                    $('body,html').animate({
                        scrollTop: 0
                    }, 800);
                    return false;
                });

            $('.toscroll').click(function() {
                var sn_id = $(this).attr('data-id');
                $('html, body').animate({
                    scrollTop: $("#"+ sn_id).offset().top
                }, 1000);
            });

        });
    </script>
    </body>
</html>