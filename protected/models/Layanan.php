<?php

/**
 * This is the model class for table "tb_layanan".
 *
 * The followings are the available columns in table 'tb_layanan':
 * @property integer $id
 * @property string $titles
 * @property string $intro
 * @property string $content
 * @property string $image
 * @property string $custom_link
 * @property integer $aktif
 * @property integer $sortings
 * @property string $tgl_input
 */
class Layanan extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Layanan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_layanan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('titles', 'required'),
			array('aktif, sortings', 'numerical', 'integerOnly'=>true),
			array('titles, image, custom_link', 'length', 'max'=>225),
			// The following rule is used by search().
			array('intro, content, image, custom_link, aktif, sortings, tgl_input', 'safe'),
			// Please remove those attributes that should not be searched.
			array('id, titles, intro, content, image, custom_link, aktif, sortings, tgl_input', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'titles' => 'Judul',
			'intro' => 'Intro Dekripsi',
			'content' => 'Konten Deskripsi',
			'image' => 'Foto',
			'custom_link' => 'Link Custom',
			'aktif' => 'Aktif',
			'sortings' => 'Urutan',
			'tgl_input' => 'Tanggal Input',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('titles',$this->titles,true);
		$criteria->compare('intro',$this->intro,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('custom_link',$this->custom_link,true);
		$criteria->compare('aktif',$this->aktif);
		$criteria->compare('sortings',$this->sortings);
		$criteria->compare('tgl_input',$this->tgl_input,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}