<?php
$this->breadcrumbs=array(
	'Appointments'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Appointment',
	'subtitle'=>'Edit Appointment',
);

$this->menu=array(
	array('label'=>'List Appointment', 'icon'=>'th-list','url'=>array('index')),
	// array('label'=>'Add Appointment', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Appointment', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model, 'm_unit'=> $m_unit)); ?>