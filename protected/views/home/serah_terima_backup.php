<div class="outer-wrapper-boxed">
	<div class="middles_content boxed_registers">
		
		<!-- Start multi step -->
		<div class="container-fluid" id="grad1">
		    <div class="row justify-content-center mt-0">
		        <div class="col-11 col-sm-9 col-md-9 col-lg-7 text-center p-0 mt-3 mb-2">
		            <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
		                <h2><strong>Registration and Handover Schedule</strong></h2>

		                <div class="row">
		                    <div class="col-md-12 mx-0">
		                        <form id="msform" method="post" class="customs_form" action="" enctype="multipart/form-data">
		                            <ul id="progressbar">
		                                <li class="active" id="account"><strong>Step 1</strong></li>
		                                <li id="personal"><strong>Step 2</strong></li>
		                                <li id="payment"><strong>Step 3</strong></li>
		                                <li id="confirm"><strong>Finish</strong></li>
		                            </ul>
		                            <fieldset>
		                                <div class="form-card">
		                                    <h2 class="fs-title">Unit Information</h2> 
		                                    <div class="form-group">
		                                    	<label>Project</label>
		                                    	<input type="text" name="Apt[proyek]" value="Citraland Utara Surabaya" readonly="readonly" />
		                                    </div>
		                                    <?php 
		                                    $nxs_cluster = Cluster::model()->findAll();
		                                    ?>
		                                    <div class="form-group">
		                                    	<label>Cluster</label>
		                                    	<select name="Apt[cluster]" id="nsx_cluster">
		                                    		<?php foreach ($nxs_cluster as $key => $value): ?>
		                                    		<option value="<?php echo $value->nama; ?>"><?php echo $value->nama ?></option>
		                                    		<?php endforeach ?>
		                                    	</select>
		                                    </div>

		                                    <div class="form-group">
		                                    	<label>Validation KTP Number</label>
		                                    	<input type="text" name="Apt[no_ktp]" class="no_ktp" placeholder="Check No. KTP" />
		                                    	<div class="py-1"></div>
		                                    	<button id="searchs_ktp" class="btn btn-info btsn_searchs">SEARCH <i class="fa fa-search"></i></button>
		                                    </div>
											<!-- select unit -->
											<div class="box-sel-unit" style="display: none;">
												<div class="form-group">
													<label>Pilih Unit Anda</label>
													<select required="required" name="Apt[select_unit]" class="form-control selec_units">
													</select>
												</div>
											</div>

		                                    <div class="block-unit" style="display: none;">
		                                    	<h6><strong>Unit Information</strong></h6>
		                                    	<p class="info_houses">
		                                    		<span class="names">Nama:</span> <br>
		                                    		<span class="address"></span>
		                                    	</p>
			                                    <input type="hidden" name="Apt[unit_id]" class="set_unitid" value="">
		                                    </div>

		                                </div> 
		                                <input type="button" name="next" class="next action-button" value="Next" />
		                            </fieldset>
		                            <fieldset>
		                                <div class="form-card">
		                                    <h2 class="fs-title">Select Appointment Date and Time</h2> 
		                                    
		                                    <div class="form-group">
		                                    	<label>Date</label>
		                                    	<input type="text" name="Apt[book_datetime]" required="required" class="datepicker_def books_date_nem" readonly="readonly">
		                                    </div>
		                                    <div class="form-group">
		                                    	<label>Time</label>
		                                    	<select name="Apt[start_datetime]" required="required" class="timepicker_def" id="">
		                                    		<option value="">Choose Time</option>
		                                    		<option value="08:00">08:00</option>
		                                    		<option value="09:00">09:00</option>
		                                    		<option value="10:00">10:00</option>
		                                    		<option value="11:00">11:00</option>
		                                    		<option value="13:00">13:00</option>
		                                    		<option value="14:00">14:00</option>
		                                    		<option value="15:00">15:00</option>
		                                    		<option value="16:00">16:00</option>
		                                    	</select>
		                                    </div>
		                                </div> 
		                                <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> 
		                                <input type="button" name="next" class="next action-button filter_date_appoint" value="Next" />
		                            </fieldset>
		                            <fieldset>
		                                <div class="form-card">
		                                    <h2 class="fs-title">Customer Information</h2>

		                                    <div class="block-unit" style="display: none;">
		                                    	<h6><strong>Unit Information</strong></h6>
		                                    	<p class="info_houses">
		                                    		<span class="names">Nama:</span> <br>
		                                    		<span class="phones"></span> <br>
		                                    		<span class="address"></span> <br>
		                                    		<span class="projects">Citraland Utara Surabaya</span>
		                                    	</p>
		                                    </div>

		                                    <div class="form-group">
												<label>Email Anda</label>
												<input type="email" name="Apt[email]" class="email" required="required" />
											</div>
											<div class="form-group">
												<label>Phone 2</label>
												<input type="text" name="Apt[phone2]" class="phone2"/>
											</div>

		                                    <div class="form-group">
		                                    	<label>Customer Attendence?</label> <br>
		                                    	<div class="form-check form-check-inline">
												  <input class="form-check-input set_konsumen_sama" type="radio" required="" name="Apt[konsumen_sama]" id="inlineRadio1" value="0">
												  <div class="py-1"></div>
												  <label class="form-check-label" for="inlineRadio1">Customer</label>
												</div>
												<div class="form-check form-check-inline">
												  <input class="form-check-input set_konsumen_sama" type="radio" required="" name="Apt[konsumen_sama]" id="inlineRadio2" value="1">
												  <label class="form-check-label" for="inlineRadio2">Other</label>
												</div>
		                                    </div>

		                                    <div class="blocks_surat_kuasa" style="display: none;">
			                                    <div class="form-group">
			                                    	<label>Upload Surat Kuasa</label>
			                                    	<input type="file" class="surat_kuasa_f" name="Apt[surat_kuasa]"  accept=".jpeg">
			                                    	<div class="clear"></div>
			                                    	<div class="py-1"></div>
			                                    	<p class="help-block">Note. filetype Jpeg & PDF, file must under 1Mb, Check example draft <a target="_blank" href="<?php echo Yii::app()->baseUrl.'/images/contoh/sk_kuasa.pdf' ?>">click here</a></p>
			                                    </div>
			                                    <div class="form-group">
			                                    	<label>Name</label>
			                                    	<input type="text" name="Apt[name_kuasa]" class="nama_kuasa">
			                                    </div>
			                                    <div class="form-group">
			                                    	<label>Phone</label>
			                                    	<input type="text" name="Apt[phone_kuasa]" class="phone_kuasa">
			                                    </div>		  
		                                    </div>                                  
		                                </div> 
		                                <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> 
		                                <input type="submit" name="make_payment" class="action-button submit_form" value="Confirm" />
		                            </fieldset>

		                            <fieldset class="last-box-form">
		                                <div class="form-card">
		                                    <h2 class="fs-title text-center"><i class="fa fa-check-circle color_green fz20"></i> Success !</h2> <br>
		                                    <div class="row justify-content-center">
		                                        <div class="col-9 text-center blocks_outers_infosuccess">
		                                            <h5>Your Appointment has been successfully booked!.</h5>
		                                            <p>Please check your email!.</p>
		                                            <div class="py-2"></div>
		                                            <?php if (isset($_GET['code']) && $_GET['code'] != ''): ?>
		                                            <?php
		                                            $no_trans = base64_decode($_GET['code']);
		                                            $data_appoint = Appointment::model()->find('transaction_no = :trans_no', array(':trans_no'=>$no_trans));
		                                            $data_unit = UnitMaster::model()->findByPk($data_appoint->unit_id);
		                                            ?>
		                                            <div class="box-fix-active_appointmentf">
			                                            <h4>Confirm Appointment</h4>
			                                            <div class="py-1"></div>
			                                            <div class="row text-left">
			                                            	<div class="col-12 col-md-6">
			                                            		<p><span>Project:</span><br> <b>Citraland Utara Surabaya</b><br>
			                                            		<span>Cluster:</span><br> <b><?php echo $data_unit->project ?></b><br>
			                                            		Schedule handover:<br><b><?php echo date('d/m/Y H:i', strtotime($data_appoint->start_datetime)); ?></b></p>
			                                            	</div>
			                                            	<div class="col-12 col-md-6">
			                                            		<div class="information_user">
			                                            			<p>Name:<br>
			                                            				<b><?php echo $data_unit->nama_pemilik ?></b> <br>
			                                            				<?php if (isset($data_appoint->kuasa_file) && isset($data_appoint->kuasa_nama)): ?>
			                                            				Nama yg Hadir: <?php echo $data_appoint->kuasa_nama ?><br>
			                                            				Phone: <?php echo $data_appoint->phone2 ?><br>
			                                            				Email: <?php echo $data_appoint->email ?><br><br>
			                                            				<?php endif ?>

			                                            				<?php if (empty($data_appoint->kuasa_nama)): ?>
			                                            				Phone: <?php echo $data_unit->phone ?>&nbsp;<br>
			                                            				Email: <?php echo $data_appoint->email ?><br>
			                                            				<?php endif ?>
			                                            				Address:&nbsp;<?php echo $data_unit->blok.' / '.$data_unit->kav.' Type '. $data_unit->tipe_rumah ?><br>
			                                            				City: Surabaya
			                                            				<br>BSC Name: <?php echo $data_unit->bsc_marketing ?><br>
			                                            				BSC Phone: <?php echo $data_unit->bsc_phone ?>
			                                            			</p>
			                                            		</div>
			                                            	</div>
			                                            </div>
		                                            	<div class="clear"></div>
		                                            </div>
		                                        	<?php endif; ?>

		                                            <div class="celar"></div>
		                                        </div>
		                                    </div>
		                                </div>
		                            </fieldset>
		                        </form>

		                        <div class="p-4 blocks_data_help alert alert-info mx-5">
		                        	<p class="m-0 mb-0">Konsumen harus memastikan nomer KTP sesuai dgn yg tertera di SPT & SPPJB. Jika nomer KTP tidak sesuai, harap menghubungi: <a href="tel:62317412888">+6231-741 2888</a> (UP: Fani/Alvia/Novi) atau email <a href="mailto:marketing@citralandutara.com">marketing@citralandutara.com</a></p>
		                        </div>
		                        <div class="py-1"></div>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
		<!-- End Multi Step -->


	  <div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/themes/base/theme.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">
	$(document).ready(function(){

	// search ktp
	$('#searchs_ktp').on('click', function(){

		var ns_ktp = $("input.no_ktp").val();
		var nsx_cluster = $("select#nsx_cluster").val();
		var request = $.ajax({
		  url: "<?php echo CHtml::normalizeUrl(array('/home/searchs_ktp')); ?>?no_ktp=" + ns_ktp + "&n_cluster="+ nsx_cluster,
		  type: "GET",
		  dataType: "json",
		  success: function(msg){
		  	// console.log(msg);
		  	// return false;

		  	if (msg.status == 'error') {
		  		swal("Data invalid!.");
		  		return false;
		  	} else {
		  		swal("Data valid!.");

		  		// process
		  		$(this).attr('disabled', 'disabled');
		  		$('input.next').removeAttr('disabled');

		  		var datas = msg.data;

		  		$('.box-sel-unit').show();
		  		$('select.selec_units').empty();
		  		$('select.selec_units').append('<option value="">-- Select --</option>');

		        $.each(datas,function(key, value){
		            $('select.selec_units').append('<option value=' + value.id + '>' + value.tipe_rumah +' - '+ value.blok +' '+ value.kav + '</option>');
		        });

		  		$('.btsn_searchs').hide();
			    return false;
		    }
		  }
		});

	});

	$('select.selec_units').change(function(){
		var n_id = $(this).val();
		$('.set_unitid').val(n_id);
		// $('.block-unit').hide();

		$.ajax({
		url: "<?php echo CHtml::normalizeUrl(array('/home/getUnit')); ?>",
		type: "POST",
		data: { "unit_id": n_id },
		dataType: "json",
		  success: function(msg){
		  	if (msg.status == 'error') {
		  		swal("Unit must be selected or you may experience problems with your data");
		  		$('input.next').attr('disabled', 'disabed');
		  		return false;
		  	} else {
		  		var datas = msg.data;
		  		$('.block-unit').show();
		  		$(".info_houses .names").empty().text("Name: "+datas.nama_pemilik);
		  		$(".info_houses .phones").empty().text("Phone: "+datas.phone);
		  		$(".info_houses .address").empty().text(datas.blok+" / "+datas.kav + " Type "+ datas.tipe_rumah);
		  		var date_ends = datas.bulan_st;
		  		var arra_date = date_ends.split(" ");

		  		// filter date check if december
		  		if (parseInt(msg.bulan_no) != 12) {
		  			var maxDndate = addMonths(new Date(datas.bulan_st),1);
		  		}else{
		  			var maxDndate = new Date(datas.bulan_st);
		  		}

		  		var set_awal;
		  		var min_dates;
		  		if (parseInt(msg.bulan_no) == parseInt(msg.bulan_sekarang)) {
		  			set_awal = "+7d";
		  			min_dates = "<?php echo date("Y-m-d", strtotime("+7 day") ); ?>";
		  		} else {
		  			set_awal = new Date("01/"+arra_date[1]+"/"+arra_date[2]);
		  			min_dates = new Date("01/"+arra_date[1]+"/"+arra_date[2]);
		  		}
		  		$('.books_date_nem').attr('min', min_dates);
		  		// filter min +7day
		  		// console.log(msg.bulan_no + " --- "+msg.bulan_sekarang);

		  		$('.datepicker_def').datepicker({
					'minDate': set_awal,
					'dateFormat': "yy-mm-dd",
					'maxDate': maxDndate,
					beforeShowDay: function(date) {
				        var day = date.getDay();
				        return [(day != 0), ''];
				    }
				});

		  		$('input.next').removeAttr('disabled');
			    return true;
		    }
		  }
		});

		return false;
	});

	// filters time pic
	$('.filter_date_appoint').on('click', function(){
		// filtering 7 appoint to ajax
		var unit_id = $('.set_unitid').val();
		var dates = $('.datepicker_def').val();
		var times = $('.timepicker_def').val();

		$.ajax({
		url: "<?php echo CHtml::normalizeUrl(array('/home/filter_pic')); ?>",
		type: "POST",
		data: { "unit_id": unit_id, "date": dates, "time": times },
		dataType: "json",
		  success: function(msg){
		  	if (msg.status == 'error') {
		  		swal("Mohon Maaf ganti jadwal anda, PIC sudah ada jadwal pada waktu tersebut!");
		  		$('input, select').attr('disabled', 'disabled');
		  		setInterval(function(){ 
		    		location.reload();
		  		}, 3000);
		  		return false;
		  	} else {
			    return true;
		    }
		  }
		});
	});

	// filters surat kuasa
	$('.set_konsumen_sama').on('click', function(){
		var n_cols = $(this).val();
		if (parseInt(n_cols) == 1) {
			$('.blocks_surat_kuasa').show();
			$('.surat_kuasa_f').attr('required', 'required');
			$('input.nama_kuasa').attr('required', 'required');
			$('input.phone_kuasa').attr('required', 'required');
		} else {
			$('.blocks_surat_kuasa').hide();
			$('.surat_kuasa_f').removeAttr('required');
			$('input.nama_kuasa').removeAttr('required');
			$('input.phone_kuasa').removeAttr('required');
		}
	});

	<?php // if(Yii::app()->user->hasFlash('success')): ?>
	<?php // endif ?>
	
	<?php if (isset($_GET['status']) && $_GET['status'] == 'success_ap'): ?>
		$('form.customs_form fieldset').hide();
		$('fieldset.last-box-form').show();
		$("#progressbar li").addClass('active');
	<?php endif ?>
	
	$('.submit_form').on('click', function(){
		
		// 	var image = $('.surat_kuasa_f').val();
			//        var base64ImageContent = image.replace(/^data:image\/(jpeg);base64,/, "");
			//        $('input.file_surat_upload').val(base64ImageContent);

			// 	// save ajaxing
			// 	$.ajax({
			//            url: '<?php // echo CHtml::normalizeUrl(array('/home/saveAppointment')); ?>',
			//            type : "POST", 
			//            dataType : 'json',
			//            cache: false,
			//            data : $("#msform").serialize(),
			//            success : function(result) {
			//                // console.log(result);
			//                if (result.status == 'error') {
			// 		  		swal("Gagal Save, Coba Lagi");
			// 		  	}else{
			// 		  		swal("Terima kasih telah membuat jadwal Serah Terima Unit Anda. Konfirmasi Jadwal Serah Terima telah terkirim ke alamat email anda.");
			// 		  	}
			//            },
			//            error: function(xhr, resp, text) {
			//                console.log(xhr, resp, text);
			//            }
			//        });

		// console.log(ndump);
		// return false;
		});

	function isEveryInputEmpty() {
	    var allEmpty = true;
	    $(':input').each(function() {
	        if ($(this).val() !== '') {
	            allEmpty = false;
	            return false; // we've found a non-empty one, so stop iterating
	        }
	    });
	    return allEmpty;
	}

	function addMonths(date, months) {
	    var d = date.getDate();
	    date.setMonth(date.getMonth() + +months);
	    if (date.getDate() != d) {
	      date.setDate(0);
	    }
	    return date;
	}


	$('input.next').attr('disabled', 'disabed');

	// Customs widget Progress
	var current_fs, next_fs, previous_fs; //fieldsets
	var opacity;
	$(".next").click(function(){
		current_fs = $(this).parent();
		next_fs = $(this).parent().next();

		//Add Class Active
		$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

		//show the next fieldset
		next_fs.show();
		//hide the current fieldset with style
		current_fs.animate({opacity: 0}, {
			step: function(now) {
				// for making fielset appear animation
				opacity = 1 - now;

				current_fs.css({
				'display': 'none',
				'position': 'relative'
				});
				next_fs.css({'opacity': opacity});
			},
			duration: 600
		});
	});

	$(".previous").click(function(){

		current_fs = $(this).parent();
		previous_fs = $(this).parent().prev();

		//Remove class active
		$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

		//show the previous fieldset
		previous_fs.show();

		//hide the current fieldset with style
		current_fs.animate({opacity: 0}, {
		step: function(now) {
			// for making fielset appear animation
			opacity = 1 - now;

			current_fs.css({
				'display': 'none',
				'position': 'relative'
				});

				previous_fs.css({'opacity': opacity});
		},
			duration: 600
		});
	});

	$('.radio-group .radio').click(function(){
		$(this).parent().find('.radio').removeClass('selected');
		$(this).addClass('selected');
	});

	});
</script>