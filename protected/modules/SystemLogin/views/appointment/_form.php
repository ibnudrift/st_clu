<?php 
$session = new CHttpSession;
$session->open();
$checks_user = $session['login']['group_id'];
?>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'appointment-form',
    'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="widget">
<h4 class="widgettitle">Data Appointment</h4>
<div class="widgetcontent">
	
	<?php echo $form->textFieldRow($model,'transaction_no',array('class'=>'span5','maxlength'=>255, 'readonly'=>'readonly')); ?>

	<?php // echo $form->textFieldRow($model,'book_datetime',array('class'=>'span5', 'readonly'=>'readonly')); ?>
	<?php if ($checks_user != 8): ?>
	<?php echo $form->textFieldRow($model,'start_datetime', array('class'=>'span5 datepicker_time')); ?>
	<?php else: ?>
	<?php echo $form->textFieldRow($model,'start_datetime',array('class'=>'span5', 'readonly'=>'readonly')); ?>
	<?php endif ?>

	<?php $saveunit_id = $model->unit_id; ?>
	<?php 
	$units = UnitMaster::model()->findByPk($model->unit_id);
	$model->info_unit = 'Nama: '.$units->nama_pemilik . "\n". 'Type '. ucwords(strtolower($units->tipe_rumah)). ' '. $units->blok.' '. $units->kav;
	?>
	<?php echo $form->textAreaRow($model,'info_unit',array('class'=>'span5', 'readonly'=>'readonly')); ?>

	<?php 
		$model->unit_id = $saveunit_id;
		echo $form->hiddenField($model,'unit_id',array('class'=>'span5')); 
	?>

	<?php if ($checks_user != 8): ?>
	<?php echo $form->radioButtonListRow($model,'st_lunas', array('0'=>'Belum Lunas', '1'=> 'Lunas'),array('class'=>'span5 stc_lunas')); ?>
	<?php echo $form->textFieldRow($model,'tgl_st_lunas', array('class'=>'span5 datepicker',)); ?>
	<?php else: ?>

		<?php $nst_lunas = $model->st_lunas; ?>
		<?php echo $form->dropDownListRow($model,'st_lunas', array('0'=>'Belum Lunas', '1'=> 'Lunas'),array('class'=>'span5', 'disabled'=>'disabled')); ?>

		<?php $model->st_lunas =  $nst_lunas; ?>
		<?php echo $form->hiddenField($model,'st_lunas',array('class'=>'span5'));  ?>
	<?php endif ?>

	<?php echo $form->radioButtonListRow($model,'status_st', array('0'=>'Belum ST', '1'=> 'Sudah ST'),array('class'=>'span5 set_status_st')); ?>

	<?php echo $form->textFieldRow($model,'tgl_set_st', array('class'=>'span5 datepicker',)); ?>
	
	<?php echo $form->textAreaRow($model,'notes',array('rows'=>3, 'class'=>'span8')); ?>

	<?php echo $form->DropDownListRow($model,'prg_komplain', array('1'=> 'Komplain', '0'=>'Tidak Komplain'),array('class'=>'span5 set_komplain_stjn')); ?>

	<?php 
		$datan_f_kompl = [
							[
							'sts_komplain_sebelum',
							'tgl_komplain_sebelum',
							'note_komplain_sebelum',
							],
							[
							'sts_komplain_sesudah',
							'tgl_komplain_sesudah',
							'note_komplain_sesudah',
							],
							// [
							// 'sts_komplain_selesai',
							// 'tgl_komplain_selesai',
							// 'note_komplain_selesai',
							// ],
					   ];
	?>
	<div class="show_komplain_unit">
		<div class="divider5"></div>
		<hr>
		<div class="divider5"></div>
		<div class="row-fluid">
			<div class="span6">
				<?php echo $form->dropDownListRow($model, $datan_f_kompl[0][0], array('0'=>'Tidak', '1'=>'Ya'), array('class'=>'span11')); ?>	
				<?php echo $form->textFieldRow($model, $datan_f_kompl[0][1], array('class'=>'span11 datepicker')); ?>	
				<?php echo $form->textAreaRow($model, $datan_f_kompl[0][2], array('class'=>'span11', 'rows'=> '6')); ?>	
			</div>
			<div class="span6">
				<?php echo $form->dropDownListRow($model, $datan_f_kompl[1][0], array('0'=>'Tidak', '1'=>'Ya'), array('class'=>'span11')); ?>	
				<?php echo $form->textFieldRow($model, $datan_f_kompl[1][1], array('class'=>'span11 datepicker')); ?>	
				<?php echo $form->textAreaRow($model, $datan_f_kompl[1][2], array('class'=>'span11', 'rows'=> '6')); ?>	
			</div>
			<?php /*
			<div class="span4">
				<?php echo $form->dropDownListRow($model, $datan_f_kompl[2][0], array('0'=>'Tidak', '1'=>'Ya'), array('class'=>'span11')); ?>	
				<?php echo $form->textFieldRow($model, $datan_f_kompl[2][1], array('class'=>'span11 datepicker')); ?>	
				<?php echo $form->textAreaRow($model, $datan_f_kompl[2][2], array('class'=>'span11')); ?>
			</div>*/ ?>
		</div>
		
		<hr>
		<div class="divider5"></div>
	</div>

	<?php echo $form->dropDownListRow($model, 'sts_salesforce', array('0'=>'Belum input sales force', '1'=>'Sudah input sales force'), array('class'=>'span3')); ?>	
	
	<?php echo $form->dropDownListRow($model, 's_smarthome', array('0'=>'Belum order', '1'=>'Sudah order', '2'=>'Sudah diterima konsumen' ), array('class'=>'span3 set_smarthm')); ?>
	
	<div class="dblock_smart">
		<?php 
		if (isset($model->s_smarthome_tgl)) {
			$model->s_smarthome_tgl = date("Y-m-d", strtotime($model->s_smarthome_tgl));
		}
		?>
		<?php echo $form->textFieldRow($model,'s_smarthome_tgl', array('class'=>'span5 datepicker fow_smarthome',)); ?>
	</div>

	<?php 
	$model->bsc_name = $m_unit->bsc_marketing;
	$model->bsc_phone = $m_unit->bsc_phone;
	$model->bulan_st = $m_unit->bulan_st;
	?>
	<?php echo $form->textFieldRow($model,'bsc_name',array('class'=>'span5', 'readonly'=>'readonly')); ?>
	
	<?php echo $form->textFieldRow($model,'bsc_phone',array('class'=>'span5', 'readonly'=>'readonly')); ?>

	<?php echo $form->textFieldRow($model,'bulan_st',array('class'=>'span5', 'readonly'=>'readonly')); ?>


	<?php if ($model->kuasa_nama): ?>
		<?php echo $form->textFieldRow($model,'kuasa_nama',array('class'=>'span5', 'readonly'=>'readonly')); ?>
		<?php echo $form->textFieldRow($model,'kuasa_phone',array('class'=>'span5', 'readonly'=>'readonly')); ?>
		<div class="control-group">
			<label class="control-label"></label>
			<div class="controls">
				<input type="text">
			</div>
		</div>
	<?php endif ?>


	<?php // echo $form->radioButtonListRow($model,'status_validasi', array('0'=>'Belum Validasi', '1'=> 'Validasi'),array('class'=>'span5')); ?>
	<?php // echo $form->textFieldRow($model,'sts_ok',array('class'=>'span5')); ?>

	<?php // echo $form->dropDownListRow($model,'sts_ok',array('class'=>'span5')); ?>
	<?php // echo $form->textFieldRow($model, 'prg_komplain',array('class'=>'span5')); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType'=>'submit',
		'type'=>'primary',
		'label'=>$model->isNewRecord ? 'Add' : 'Save',
	)); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		// 'buttonType'=>'submit',
		// 'type'=>'info',
		'url'=>CHtml::normalizeUrl(array('index')),
		'label'=>'Batal',
	)); ?>
</div>
</div>
<div class="alert">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Warning!</strong> Fields with <span class="required">*</span> are required.
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
	jQuery(function($){

		// ST LUNAS
		var nst_lunas = <?php echo ($model->st_lunas != 0)? $model->st_lunas : 0; ?>;
		var in_tgl_stlunas = $('#Appointment_tgl_st_lunas');
		if( nst_lunas == "1"){
			$(in_tgl_stlunas).parent().parent().show();
			$('#uniform-Appointment_st_lunas_0').find('input').attr('disabled', 'disabled');
			$('#uniform-Appointment_st_lunas_0').find('label').css('cursor', 'default');
		}else{
			$(in_tgl_stlunas).parent().parent().hide();
		}

		$('.stc_lunas').on('click', function(){
			console.log($(this).val());
			if ($(this).val() == 1){
				$(in_tgl_stlunas).attr('required', 'required');
				$(in_tgl_stlunas).parent().parent().show();
			}else{
				$(in_tgl_stlunas).removeAttr('required');
				$(in_tgl_stlunas).parent().parent().hide();
			}
		});

		// Check ST
		var nst_check = <?php echo ($model->status_st != 0)? $model->status_st : 0; ?>;
		var in_tgl_stn_2 = $('#Appointment_tgl_set_st');
		if( nst_check == "1"){
			$(in_tgl_stn_2).parent().parent().show();
			$('#uniform-Appointment_status_st_0').find('input').attr('disabled', 'disabled');
			$('#uniform-Appointment_status_st_0').find('label').css('cursor', 'default');
		}else{
			$(in_tgl_stn_2).parent().parent().hide();
		}

		$('.set_status_st').on('click', function(){
			console.log($(this).val());
			if ($(this).val() == 1){
				$(in_tgl_stn_2).attr('required', 'required');
				$(in_tgl_stn_2).parent().parent().show();
			}else{
				$(in_tgl_stn_2).removeAttr('required');
				$(in_tgl_stn_2).parent().parent().hide();
			}
		});

		// view block komplain
		var sin_komp = $('.set_komplain_stjn');
		var blok_unit_komplain = $('.show_komplain_unit');

		if ($(sin_komp).val() == "1") {
			$(blok_unit_komplain).show();
		}else{
			$(blok_unit_komplain).hide();
		}

		$(sin_komp).change(function(){
			if ($(this).val() == "1"){
				$(blok_unit_komplain).show();
			}else{
				$(blok_unit_komplain).hide();
			}
		});

		<?php if ($model->s_smarthome != 2): ?>
			$('.dblock_smart').hide();
			$('.set_smarthm').change(function(){
				if (parseInt($(this).val()) == 2 ){
					$('.dblock_smart').show();
				}else{
					$('.dblock_smart').hide();
				}
			});
		<?php endif ?>

	});
</script>