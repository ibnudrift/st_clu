<?php
$this->breadcrumbs=array(
	'Layanan'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Layanan',
	'subtitle'=>'Add Layanan',
);

$this->menu=array(
	array('label'=>'List Layanan', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>