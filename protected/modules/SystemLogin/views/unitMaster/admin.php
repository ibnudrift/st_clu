<?php
$this->breadcrumbs=array(
	'Unit Masters'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List UnitMaster','url'=>array('index')),
	array('label'=>'Add UnitMaster','url'=>array('create')),
);
?>

<h1>Manage Unit Masters</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'unit-master-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'member_id',
		'ktp_no',
		'blok',
		'kav',
		'nama_pemilik',
		/*
		'tipe_rumah',
		'jadwal_st_kontraktor',
		'jadwal_st_web',
		'bsc_marketing',
		'created_at',
		'updated_at',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
