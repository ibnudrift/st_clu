<?php
$this->breadcrumbs=array(
	'Bsc'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Bsc',
	'subtitle'=>'Add Bsc',
);

$this->menu=array(
	array('label'=>'List Bsc', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>