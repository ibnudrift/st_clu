<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('transaction_no')); ?>:</b>
	<?php echo CHtml::encode($data->transaction_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('book_datetime')); ?>:</b>
	<?php echo CHtml::encode($data->book_datetime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('start_datetime')); ?>:</b>
	<?php echo CHtml::encode($data->start_datetime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('end_datetime')); ?>:</b>
	<?php echo CHtml::encode($data->end_datetime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('notes')); ?>:</b>
	<?php echo CHtml::encode($data->notes); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('unit_id')); ?>:</b>
	<?php echo CHtml::encode($data->unit_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reschedules')); ?>:</b>
	<?php echo CHtml::encode($data->reschedules); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sts_ok')); ?>:</b>
	<?php echo CHtml::encode($data->sts_ok); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('prg_komplain')); ?>:</b>
	<?php echo CHtml::encode($data->prg_komplain); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_at')); ?>:</b>
	<?php echo CHtml::encode($data->created_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_at')); ?>:</b>
	<?php echo CHtml::encode($data->updated_at); ?>
	<br />

	*/ ?>

</div>