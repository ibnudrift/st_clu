<?php
$this->breadcrumbs=array(
	'Member Appointment',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Member Appointment',
	'subtitle'=>'Data Member Appointment',
);

$this->menu=array(
	array('label'=>'Add Member Appointment', 'icon'=>'plus-sign','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>
    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>
<?php endif; ?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'member-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'htmlOptions'=> array('class'=>'custom_table'),
	'columns'=>array(
		'id',
		array(
			'header'=>'Nama',
			'type'=>'raw',
			'value'=> '$data->first_name." ".$data->last_name',
		),
		array(
			'header'=>'Nama Serah Terima',
			'type'=>'raw',
			'value'=> '$data->nama_lain',
		),
		'no_ktp',
		'email',
		'phone',
		/*
		'address',
		'city',
		'state',
		'zip_code',
		'notes',
		'status_lock',
		'created_at',
		'updated_at',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
		),
	),
)); ?>
