<?php
$this->breadcrumbs=array(
	'Proyek',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Proyek',
	'subtitle'=>'Data Proyek',
);

$this->menu=array(
	array('label'=>'Add Proyek', 'icon'=>'plus-sign','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'proyek-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		'nama_proyek',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			// 'template'=>'{update} &nbsp; {delete}',
			'template'=>'{update}',
		),
	),
)); ?>
