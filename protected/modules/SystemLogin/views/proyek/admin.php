<?php
$this->breadcrumbs=array(
	'Proyeks'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Proyek','url'=>array('index')),
	array('label'=>'Add Proyek','url'=>array('create')),
);
?>

<h1>Manage Proyeks</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'proyek-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'nama_proyek',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
