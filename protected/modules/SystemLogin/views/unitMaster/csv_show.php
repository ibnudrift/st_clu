<?php
$this->breadcrumbs=array(
	'Unit Master'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-life-ring',
	'title'=>'Unit Master',
	'subtitle'=>'Add Unit Master',
);

$this->menu=array(
	// array('label'=>'List Unit Master', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'input-product-form',
    // 'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<div class="row-fluid">
	<div class="span12">
		<!-- ----------------- Action ----------------- -->
		<div class="widgetbox block-rightcontent">                        
		    <div class="headtitle">
		        <h4 class="widgettitle">Data File CSV</h4>
		    </div>
		    <div class="widgetcontent">

				<?php $this->widget('bootstrap.widgets.TbButton', array(
					'buttonType'=>'submit',
					'type'=>'primary',
					'label'=>'Update Data',
					'htmlOptions'=>array('class'=>'btn-large', 'value'=>'submit', 'name'=>'submit'),
				)); ?>
				<?php $this->widget('bootstrap.widgets.TbButton', array(
					// 'buttonType'=>'submit',
					// 'type'=>'info',
					'url'=>CHtml::normalizeUrl(array('index')),
					'label'=>'Cancel',
					'htmlOptions'=>array('class'=>'btn-large'),
				)); ?>

			<div id="or-order-grid" class="grid-view">
			   <div class="summary"></div>
			   <table class="items table table-bordered">
			      <thead>
			         <tr>
						<th>KTP</th>
						<th>Blok</th>
						<th>Kav</th>
						<th>Tipe rumah</th>
						<th>Nama pemilik</th>
						<th>Phone</th>
						<th>Jadwal ST web</th>
						<th>BSC</th>
						<th>Project</th>
			         </tr>
			      </thead>
			      <tbody>
			      	<?php foreach ($dataCsv as $key => $value): ?>
			         <tr>
						<td><?php echo $value['0'] ?></td>
						<td><?php echo $value['1'] ?></td>
						<td><?php echo $value['2'] ?></td>
						<td><?php echo $value['3'] ?></td>
						<td><?php echo $value['4'] ?></td>
						<td><?php echo $value['5'] ?></td>
						<td><?php echo $value['6'] ?></td>
						<td><?php echo $value['7'] ?></td>
						<td><?php echo $value['10'] ?></td>
			         </tr>
			      	<?php endforeach; ?>
			      </tbody>
			   </table>
			</div>


				<?php $this->widget('bootstrap.widgets.TbButton', array(
					'buttonType'=>'submit',
					'type'=>'primary',
					'label'=>'Update Data',
					'htmlOptions'=>array('class'=>'btn-large', 'value'=>'submit', 'name'=>'submit'),
				)); ?>
				<?php $this->widget('bootstrap.widgets.TbButton', array(
					// 'buttonType'=>'submit',
					// 'type'=>'info',
					'url'=>CHtml::normalizeUrl(array('index')),
					'label'=>'Cancel',
					'htmlOptions'=>array('class'=>'btn-large'),
				)); ?>
		    </div>
		</div>
	</div>
</div>

<?php $this->endWidget(); ?>

