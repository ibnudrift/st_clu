<?php
$this->breadcrumbs=array(
	'Bsc',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Bsc',
	'subtitle'=>'Data Bsc',
);

$this->menu=array(
	array('label'=>'Add Bsc', 'icon'=>'plus-sign','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>
<!-- <h1>Bsc</h1> -->
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'bsc-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'htmlOptions'=> array('class'=>'custom_table'),
	'columns'=>array(
		// 'id',
		'nickname',
		'nama_bsc',
		'phone',
		'email',
		/*
		'created_at',
		'updated_at',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
		),
	),
)); ?>
