<?php
$this->breadcrumbs=array(
	'Clusters',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Cluster',
	'subtitle'=>'Data Cluster',
);

$this->menu=array(
	array('label'=>'Add Cluster', 'icon'=>'plus-sign','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>
<!-- <h1>Cluster</h1> -->
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'cluster-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		'nama',
		// 'proyek_id',
		array(
			'header'=>'Proyek',
			'type'=>'raw',
			'value'=>'Proyek::model()->findByPk($data->proyek_id)->nama_proyek',
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
		),
	),
)); ?>
