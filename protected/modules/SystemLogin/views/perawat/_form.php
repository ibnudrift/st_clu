<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'perawat-form',
    'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="widget">
<h4 class="widgettitle">Data Perawat</h4>
<div class="widgetcontent">
	<?php
	$models_layanan = Layanan::model()->findAll();   
	// $data_list = CHtml::listData($models, 'id', 'titles');    
	?>
	<?php // echo $form->dropDownListRow($model,'tipe_perawat_id', $data_list, array('class'=>'span5', 'multiple'=>'multiple')); ?>

	<div class="control-group ">
		<label class="control-label" for="Perawat_tipe_perawat_id">Tipe Perawat</label>
		<div class="controls">

			<select class="span5" multiple="multiple" name="Perawat[tipe_perawat_id][]" id="Perawat_tipe_perawat_id" required>
				<?php if ($model->scenario == 'update' && $model->tipe_perawat_id != ''): ?>
					<?php $model->tipe_perawat_id = unserialize($model->tipe_perawat_id); ?>
					<?php if (is_array($model->tipe_perawat_id) && $model->tipe_perawat_id != ''): ?>
						<?php foreach ($model->tipe_perawat_id as $ke => $val): ?>
						<option selected value="<?php echo $val; ?>"><?php echo Layanan::model()->findByPk($val)->titles; ?></option>
						<?php endforeach ?>
					<?php endif ?>

					<?php if (is_array($model->tipe_perawat_id) && $model->tipe_perawat_id != ''): ?>
						<?php 
						$key_exists = array();
						foreach ($models_layanan as $key => $value) {
							$key_exists[] = $value->id;
						}
						$result_diff = array_diff($key_exists, $model->tipe_perawat_id);
						?>
						<?php foreach ($result_diff as $key => $value): ?>
							<?php $mod_diff = Layanan::model()->findByPk($value); ?>
							<option value="<?php echo $mod_diff->id; ?>"><?php echo $mod_diff->titles; ?></option>
						<?php endforeach; ?>
					<?php endif ?>
				<?php else: ?>
					<?php foreach ($models_layanan as $key => $value): ?>
						<option value="<?php echo $value->id; ?>"><?php echo $value->titles; ?></option>
					<?php endforeach; ?>
				<?php endif ?>
			</select>

		</div>
	</div>

	<?php echo $form->textFieldRow($model,'nama',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->dropDownListRow($model,'jenis_kelamin', [ 'laki-laki'=>'Laki-Laki', 'perempuan'=>'Perempuan' ] , array('class'=>'span5')); ?>

	<?php
	$d_agama = [
				'islam'=>'Islam',
				'kristen'=>'Kristen',
				'hindu'=>'Hindu',
				'budha'=>'Budha',
				];
	?>
	<?php echo $form->dropDownListRow($model,'agama', $d_agama, array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'pendidikan',array('class'=>'span5','maxlength'=>25)); ?>

	<?php echo $form->textFieldRow($model,'tinggi_badan',array('class'=>'span5','maxlength'=>25)); ?>

	<?php echo $form->textFieldRow($model,'berat_badan',array('class'=>'span5','maxlength'=>25)); ?>

	<?php echo $form->textAreaRow($model,'sertifikat',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'pengalaman',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'penempatan',array('class'=>'span5','maxlength'=>225)); ?>

	<?php 
	$d_provinsi = CHtml::listData(City::getProvince(), 'province_id', 'province');
	echo $form->dropDownListRow($model,'provinsi', $d_provinsi, array('class'=>'span5 call_city','maxlength'=>225));
	?>

	<?php echo $form->dropDownListRow($model,'kota', array(), array('class'=>'span5 dv_city','maxlength'=>225)); ?>


	<?php echo $form->textFieldRow($model,'pengalaman_lama',array('class'=>'span5', 'hint'=>'ex. 2 tahun')); ?>

	<?php echo $form->fileFieldRow($model,'image',array(
	'hint'=>'<b>Note:</b> The image size is 1209 x 790px. Larger images will be cropped automatically, please upload photos of horizontal size')); ?>
	<?php if ($model->scenario == 'update'): ?>
	<div class="control-group">
		<label class="control-label">&nbsp;</label>
		<div class="controls">
		<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(275,186, '/images/intern_perawat/'.$model->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>"/>
		</div>
	</div>
	<?php endif; ?>

	<?php echo $form->dropDownListRow($model, 'aktif', array(
        		'1'=>'Di Tampilkan',
        		'0'=>'Di Sembunyikan',
        	)); ?>

	<?php 
	$d_kontrak = [
		'harian'=>'harian',
		'bulan'=>'bulanan',
		];
	?>
	<div class="control-group ">
		<label class="control-label" for="Perawat_durasi_kontrak_max">Durasi Kontrak</label>
		<div class="controls">
			
			<select class="span5" multiple="multiple" name="Perawat[durasi_kontrak_max][]" id="Perawat_durasi_kontrak_max" required>
				<?php if ($model->scenario == 'update' && $model->durasi_kontrak_max != ''): ?>
					<?php $model->durasi_kontrak_max = unserialize($model->durasi_kontrak_max); ?>
					<?php if (is_array($model->durasi_kontrak_max) && $model->durasi_kontrak_max != ''): ?>
						<?php foreach ($model->durasi_kontrak_max as $ke => $val): ?>
						<option selected value="<?php echo $val; ?>"><?php echo $val ?></option>
						<?php endforeach ?>
					<?php endif; ?>

					<?php if (is_array($model->durasi_kontrak_max) && $model->durasi_kontrak_max != ''): ?>
						<?php 
						$key_exists2 = array();
						foreach ($d_kontrak as $key => $value) {
							$key_exists2[] = $key;
						}
						$result_diff2=array_diff($key_exists2, $model->durasi_kontrak_max);
						?>
						<?php foreach ($result_diff2 as $key => $value): ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
						<?php endforeach; ?>
					<?php endif; ?>
				<?php else: ?>
					<?php foreach ($d_kontrak as $key => $value): ?>
						<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
					<?php endforeach; ?>
				<?php endif ?>
			</select>

		</div>
	</div>

		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Add' : 'Save',
		)); ?>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			// 'buttonType'=>'submit',
			// 'type'=>'info',
			'url'=>CHtml::normalizeUrl(array('index')),
			'label'=>'Batal',
		)); ?>
</div>
</div>
<div class="alert">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Warning!</strong> Fields with <span class="required">*</span> are required.
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
jQuery(function($){

	$('.call_city').live('change', function() {
	$.ajax({
		type: "POST",
		url: "<?php echo CHtml::normalizeUrl(array('/SystemLogin/perawat/getTo')) ?>",
		dataType: "html",
		data: { province: $('.call_city').val()}
	}).done(function( msg ) {
		$(".dv_city").html(msg);
		$(".dv_city").val('<?php echo $model->kota ?>');
	})
	})

})
</script>