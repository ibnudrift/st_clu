<?php

class HomeController extends Controller
{

	public function actions()
	{
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	}

	public function actionTestmail()
	{
		// test email
		$this->layout = '//layouts/_blank';

		$model = Appointment::model()->findByPk(1);
		$m_unit = UnitMaster::model()->findByPk($model->unit_id);

		$mail = $this->renderPartial('//mail/appoint2', array(
			'model' => $model,
			'm_unit' => $m_unit,
		), true);

		$config = array(
				'to'=>array($this->setting['email'], $model->email),
				'subject'=>'['.Yii::app()->name.'] Appointment Citraland Utara '. ucwords(strtolower($m_unit->project)),
				'message'=>$mail,
				'bcc' => array('fotocopysurabaya12@gmail.com'),
			);
		print_r($config);
		// Common::mail($config);
	}

	public function actionIndex()
	{
		$this->pageTitle = ($this->setting['home_meta_title'] != '')? $this->setting['home_meta_title'] : $this->pageTitle;
		$this->metaKey = ($this->setting['home_meta_keyword'] != '')? $this->setting['home_meta_keyword'] : $this->metaKey;
		$this->metaDesc = ($this->setting['home_meta_description'] != '')? $this->setting['home_meta_description'] : $this->metaDesc;

		$this->layout='//layouts/column1';
		
		$this->render('index', array(
		));
	}

	public function actionSerahTerima()
	{
		$this->pageTitle = ($this->setting['home_meta_title'] != '')? $this->setting['home_meta_title'] : $this->pageTitle;
		$this->metaKey = ($this->setting['home_meta_keyword'] != '')? $this->setting['home_meta_keyword'] : $this->metaKey;
		$this->metaDesc = ($this->setting['home_meta_description'] != '')? $this->setting['home_meta_description'] : $this->metaDesc;

		$this->layout='//layouts/column2';

		if(isset($_POST['Apt']))
		{
			$chc_appoint = Appointment::model()->find('unit_id = :unt_id', array(':unt_id'=>$_POST['Apt']['unit_id']));
			if (empty($chc_appoint) and $_POST['Apt']['unit_id'] != '' and $_POST['Apt']['book_datetime'] != '' and $_POST['Apt']['start_datetime'] != '' ) {

				// Filter bulan tidak boleh maju, bisanya mundur bulan
				$get_bulan_st = UnitMaster::model()->find('t.id = :f_id', array(':f_id'=>$_POST['Apt']['unit_id']));
				$dt_bln_set = date("m", strtotime($get_bulan_st->bulan_st));
				$bulan_inputs = date("m", strtotime($_POST['Apt']['book_datetime']));

				if($bulan_inputs < $dt_bln_set){
					Yii::app()->user->setFlash('error_date', 'Error Date Appointment, Please select the true date');
					$this->redirect(array('/home/serahTerima'));
				}

				// Save process
				$model=new Appointment;
				if (isset($_FILES['Apt']['tmp_name']['surat_kuasa']) && !empty($_FILES['Apt']['tmp_name']['surat_kuasa'])) {
					$namaFile = $_FILES['Apt']['name']['surat_kuasa'];
					$namaSementara = $_FILES['Apt']['tmp_name']['surat_kuasa'];
					$dirUpload = Yii::getPathOfAlias('webroot').'/images/surat_kuasa/';
					$model->kuasa_file = mt_rand(10, 1000).'-'.$namaFile;
					$terupload = move_uploaded_file($namaSementara, $dirUpload.$model->kuasa_file);
				}else{
					$model->kuasa_file = null;
				}

				// $model->attributes=$_POST['Apt'];
				$model->transaction_no = 'ST-CLU-'.date("Ymd").mt_rand(10,1000);
				$model->book_datetime = $_POST['Apt']['book_datetime'].' 00:00:00';
				$model->start_datetime = $_POST['Apt']['book_datetime'].' '. $_POST['Apt']['start_datetime'].':00';
				$model->end_datetime = $_POST['Apt']['book_datetime'].' '. $_POST['Apt']['start_datetime'].':00';
				$model->notes = '';
				$model->unit_id = $_POST['Apt']['unit_id'];
				$model->user_id = 0;
				$model->reschedules = 0;
				$model->status_validasi = 0;
				$model->status_st = 0;
				$model->prg_komplain = 0;
				$model->proyek_id = 1;
				$model->phone2 = $_POST['Apt']['phone2'];
				$model->kuasa_nama = $_POST['Apt']['name_kuasa'];
				$model->kuasa_phone = $_POST['Apt']['phone_kuasa'];
				$model->email = $_POST['Apt']['email'];
				$model->created_at = date("Y-m-d H:i:s");
				$model->updated_at = date("Y-m-d H:i:s");
				$model->tgl_st_lunas = null;
				$model->tgl_set_st = null;
				$model->tgl_komplain_sebelum = null;
				$model->tgl_komplain_sesudah = null;
				$model->tgl_komplain_selesai = null;
				$model->save(false);

				// Sent Email
				$m_unit = UnitMaster::model()->findByPk($model->unit_id);
				$m_unit->email = $model->email;
				$m_unit->save(false);
				$messaged = $this->renderPartial('//mail/appoint', array(
					'model' => $model,
					'm_unit' => $m_unit,
				), true);

				$config = array(
					'to'=>array($this->setting['email'], $model->email),
					'subject'=>'['.Yii::app()->name.'] Appointment Citraland Utara '. ucwords(strtolower($m_unit->project)),
					'message'=>$messaged,
				);
				Common::mail($config);

				Yii::app()->user->setFlash('success','Success Appointment');
				$this->redirect(array('/home/serahTerima', 'status'=>'success_ap', 'code'=> base64_encode($model->transaction_no) ));
			}

		}
		
		$this->render('serah_terima', array(
		));
	}

	public function actionRescheduleST()
	{
		$this->layout='//layouts/column2';

		$no_trans = base64_decode($_GET['code']);
		$model = Appointment::model()->find('transaction_no = :trans_no', array(':trans_no'=>$no_trans));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');

		$data_unit = UnitMaster::model()->findByPk($model->unit_id);

		// filter check -2 day, not run schedules
		$n_status = true;

		$dt_now = date("Y-m-d H:i:s");
		$dt_appoint = date("Y-m-d H:i:s", strtotime($model->book_datetime));

		$startDate = new DateTime($dt_appoint);
		$diff = $startDate->diff(new DateTime($dt_now));

		if (intval($diff->d) < 2 or intval($model->reschedules) == 1) {
			$n_status = false;
		}

		if(isset($_POST['Appointment']))
		{
			$date_old = $model->book_datetime;
			$model->attributes=$_POST['Appointment'];
			
			$cekk2 = TRUE;
			$a = new DateTime($date_old);
			$b = new DateTime($model->book_datetime);
			$interval = $b->diff($a);
			$return_has = $interval->format("%r%a");

			if ($return_has > 0) {
				Yii::app()->user->setFlash('danger', Tt::t('front', 'Please input the correct date, The date must be longer'));
				$this->refresh();
			}


			if ($model->validate() && $cekk2 == TRUE) {
				
				$timestamp = strtotime($model->start_datetime) + 60*60;
				$time_end = date('H:i', $timestamp);

				$model->start_datetime = $model->book_datetime.' '. $model->start_datetime.':00';
				$model->end_datetime = $model->book_datetime.' '. $time_end.':00';
				$model->book_datetime = $model->book_datetime.' 00:00:00';

				$model->save(false);

				// sents Email
				$m_unit = UnitMaster::model()->findByPk($model->unit_id);
				$messaged = $this->renderPartial('//mail/appoint2', array(
					'model' => $model,
					'm_unit' => $m_unit,
				), true);

				$config = array(
					'to'=>array($this->setting['email'], $model->email),
					'subject'=>'['.Yii::app()->name.'] Reschedules Appointment Citraland Utara '. ucwords(strtolower($m_unit->project)),
					'message'=>$messaged,
				);
				Common::mail($config);

				Yii::app()->user->setFlash('success', Tt::t('front', 'Thank you for reschedule appointment. Please check your email.'));
				$this->refresh();
			}
		}

		$this->render('p_schedule', array(
			'model'=>$model,
			'data_unit' => $data_unit,
			'bulan_no' => date("m", strtotime($data_unit->bulan_st)),
			'st_form'=> $n_status,
		));
	}

	public function actionSearchs_ktp()
	{
		// filter ktp
		$n_ktp = $_GET['no_ktp'];
		$n_cluster = strtoupper($_GET['n_cluster']);

		// check unit yang telah appointment
		$sql = 'SELECT tb1.* FROM `unit_master` tb1 WHERE tb1.bulan_st NOT LIKE "%February%" and tb1.ktp_no = "'.$n_ktp.'" AND tb1.project = "'.$n_cluster.'" AND tb1.id NOT IN (SELECT unit_id FROM `appointment`)';
		$m_searchs = Yii::app()->db->createCommand($sql)->queryAll();

		if ($m_searchs) {
			$res = [];
			foreach ($m_searchs as $key => $value){
				$res[] = $value;
			}

			$arr = array(
						'status'=>'success',
						'data' => $res,
					);
		}else{
			$arr = array(
						'status'=>'error',
						'data' => false,
					);
		}
		echo json_encode($arr);
	}

	public function actionGetUnit()
	{
		$n_unit_id = intval($_POST['unit_id']);

		$get_unit = UnitMaster::model()->find('t.id = :ids', array(':ids'=> $n_unit_id));
		if ($get_unit) {
			$arr = array(
						'status'=>'success',
						'data' => $get_unit->attributes,
						'bulan_no' => date("m", strtotime($get_unit->bulan_st)),
						'bulan_sekarang' => date("m"),
						'tahun_book' => date("Y", strtotime($get_unit->bulan_st)),
						'tahun_sekarang' => date("Y"),
					);
		}else{
			$arr = array(
						'status'=>'error',
						'data' => false,
					);
		}
		
		echo json_encode($arr);		
	}

	public function actionFilter_pic()
	{
		$n_unit_id = intval($_POST['unit_id']);
		$n_date = $_POST['date'];
		$n_time = $_POST['time'];
		
		// PIC tidak boleh lebih dari 1
		$get_bsc = UnitMaster::model()->find('t.id = :ids', array(':ids'=> $n_unit_id))->bsc_marketing;
		
		$m_searchs = Yii::app()->db->createCommand()
				    ->select('tb1.*, tb2.bsc_marketing')
				    ->from('appointment tb1')
				    ->leftJoin('unit_master tb2', 'tb1.unit_id=tb2.id')
				    ->where('DATE(tb1.book_datetime) = :date1 AND TIME(tb1.start_datetime) = :time1 and bsc_marketing = :nmarket', 
				    		array(':date1'=> $n_date, ':time1' => $n_time.':00', ':nmarket'=>$get_bsc)
				    	)
				    ->queryRow();

		if (!empty($m_searchs)) {
				$arr = array(
						'status'=>'error',
					);	
		}else{
				$arr = array(
							'status'=>'success',
						);
		}

		echo json_encode($arr);
	}

	public function actionSaveAppointment()
	{
		if(isset($_POST['Apt']))
		{
			// check apakah sudah ada appointment dengan unit_id yg sama.
			$chc_appoint = Appointment::model()->find('unit_id = :unt_id', array(':unt_id'=>$_POST['Apt']['unit_id']));
			if (empty($chc_appoint)) { 
				$model=new Appointment;
				// $model->attributes=$_POST['Apt'];
				if ($_POST['Apt']['file_surat_upload']) {
					$pic_image = base64_encode(file_get_contents($_POST['Apt']['file_surat_upload']));
					$model->kuasa_file = $pic_image;
				}

				$model->transaction_no = 'ST-CLU-'.date("Ymd").mt_rand(10,1000);
				$model->book_datetime = $_POST['Apt']['book_datetime'].' 00:00:00';
				$model->start_datetime = $_POST['Apt']['book_datetime'].' '. $_POST['Apt']['start_datetime'].':00';
				$model->end_datetime = $_POST['Apt']['book_datetime'].' '. $_POST['Apt']['start_datetime'].':00';
				$model->notes = '';
				$model->status = 0;
				$model->unit_id = $_POST['Apt']['unit_id'];
				$model->user_id = 0;
				$model->reschedules = 0;
				$model->sts_ok = 0;
				$model->prg_komplain = 0;
				$model->proyek_id = 1;
				$model->kuasa_nama = $_POST['Apt']['name_kuasa'];
				$model->kuasa_phone = $_POST['Apt']['phone_kuasa'];
				$model->created_at = date("Y-m-d H:i:s");
				$model->updated_at = date("Y-m-d H:i:s");
				$model->save(false);
			}

			$arr = array(
						'status'=>'success',
					);	
			echo json_encode($arr);
		}else{

			$arr = array(
						'status'=>'error',
					);	
			echo json_encode($arr);
		}


	}

	public function actionTestView()
	{
		$nall = Appointment::model()->findAll();

		$this->render('test_view', array(
			'nall' => $nall,
		));
	}

	public function actionEnquire()
	{
		$this->layout='//layouts/column2';
		$model = new Enquire;
		if(isset($_POST['Enquire']))
		{
			$model->attributes=$_POST['Enquire'];

			$status = true;
	        $secret_key = "6LeoESkUAAAAALvt2dGt0u_XsurphVpIWAwk8nnT";
	        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
	        $response = json_decode($response);
	        if($response->success==false)
	        {
	        	$model->addError('email', Tt::t('front', 'You must complete the captcha first.'));
	        	$status = false;
	        }

			if ($status AND $model->validate()) {
				// $model->save();

				$mail = $this->renderPartial('//mail/enquire', array(
					'model'=>$model,
				), true);

				$config = array(
					'to'=>array($this->setting['email'], $model->email),
					'bcc'=>array('info@fotocopysurabaya.web.id'),
					'subject'=>'Enquire medisku.com from '. $model->email,
					'message'=>$mail,
				);
				// echo "<pre>";
				// echo print_r($config);
				// echo "</pre>";
				// exit;

				// kirim email
				Common::mail($config);

				Yii::app()->user->setFlash('success',Tt::t('front', 'Thank you for sending contacting form.'));
				$this->redirect(array('contact'));
			}
		}
		exit;
	}

	public function actionRekapTahun()
	{
		$sql = 'SELECT tb1.* FROM `unit_master` tb1 WHERE tb1.id NOT IN (SELECT unit_id FROM `appointment`) AND bulan_st IN("30 November 2020","31 December 2020")';
		$model = Yii::app()->db->createCommand($sql)->queryAll();

		echo "rekap selesai"; exit;

		foreach ($model as $key => $value) {
			$mod_up = UnitMaster::model()->findByPk($value['id']);
			$mod_up->bulan_st = '31 January 2021';
			$mod_up->save(false);
		}

		echo "rekaps done";
		exit;
	}


	public function actionError()
	{
		$this->layout = '//layouts/error';
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else{
				$this->layout='//layouts/column2';

				$this->pageTitle = 'Error '.$error['code'].': '. $error['message'] .' - '.$this->pageTitle;
				$this->render('error', array(
					'error'=>$error,
				));
			}
		}

	}

}