<?php

$session = new CHttpSession;
$session->open();
$checks_user = $session['login']['group_id'];

$this->breadcrumbs=array(
	'Appointments',
);
$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Appointment',
	'subtitle'=>'Data Appointment',
);
$this->menu=array(
	array('label'=>'Export belum Appointment', 'icon'=>'download','url'=>array('export_belum')),
);
?>

<?php if(Yii::app()->user->hasFlash('success')): ?>
    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>
<?php endif; ?>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'htmlOptions'=>array('class'=>''),
)); ?>
	<div class="row-fluid">
		<?php /*
		<!-- <div class="span2">
			<?php 
			if (isset($_GET['Appointment']['date1'])) {
				$model->date1 = $_GET['Appointment']['date1'];
			}
			?>
			<?php echo $form->textFieldRow($model, 'date1', array('class'=>'span12 datepicker')); ?>
		</div>
		<div class="span2">
			<?php 
			if (isset($_GET['Appointment']['date2'])) {
				$model->date2 = $_GET['Appointment']['date2'];
			}
			?>
			<?php echo $form->textFieldRow($model, 'date2', array('class'=>'span12 datepicker')); ?>
		</div> -->
		*/ ?>

		<div class="span2">
			<?php echo $form->dropDownListRow($model, 'prg_komplain', array('0'=>'Tidak Komplain', '1'=>'Komplain'),array('class'=>'span12', 'empty'=>'Pilih')); ?>
		</div>
		<div class="span2">
			<?php echo $form->dropDownListRow($model, 'st_lunas', array('0'=>'Belum Lunas', '1'=>'Lunas'),array('class'=>'span12', 'empty'=>'Pilih')); ?>
		</div>
		<div class="span2">
			<?php echo $form->dropDownListRow($model, 'status_st', array('0'=>'Belum ST', '1'=>'Sudah ST'),array('class'=>'span12', 'empty'=>'Pilih')); ?>
		</div>
		<div class="span2">
			<?php echo $form->dropDownListRow($model, 's_smarthome', array('0'=>'Belum order', '1'=>'Sudah order', '2'=>'Sudah diterima konsumen' ), array('class'=>'span12', 'empty'=>'Pilih')); ?>
		</div>
		<div class="span1">
			<label>&nbsp;</label>
			<?php echo $form->checkBoxRow($model, 'to_excel', array('class'=>'span12')); ?>
		</div>
		
		<div class="span3">
			<label for="">&nbsp;</label>
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>'Submit',
			)); ?>
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'type'=>'primary',
				'label'=>'Reset',
				'url'=>Yii::app()->createUrl($this->route),
			)); ?>			
		</div>
	</div>
<?php $this->endWidget(); ?>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>

<?php if ($checks_user != 8): ?>
	<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'appointment-grid',
	'dataProvider'=>$model->search(),
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'htmlOptions'=> array('class'=>'custom_table'),
	'rowCssClassExpression' => 'Appointment::model()->getLegend_Listing($data)',
	'columns'=>array(
		array(
			'header'=>'Date Schedule',
			'type'=>'raw',
			'value'=>'date("d/m/Y H:i", strtotime($data->start_datetime) )',
		),
		array(
			'header'=>'Pemilik',
			'type'=>'raw',
			'value'=>'UnitMaster::model()->findByPk($data->unit_id)->nama_pemilik',
		),
		array(
			'header'=>'Unit Address',
			'type'=>'raw',
			'value'=>'Appointment::model()->unitAddress($data->unit_id)',
		),
		array(
			'header'=>'Cluster',
			'type'=>'raw',
			'value'=>'UnitMaster::model()->findByPk($data->unit_id)->project',
		),
		array(
			'header'=>'Bsc',
			'type'=>'raw',
			'value'=>'UnitMaster::model()->findByPk($data->unit_id)->bsc_marketing',
		),
		array(
			'header'=>'S. Lunas',
			'type'=>'raw',
			'value'=>'($data->st_lunas == "1") ? "Lunas": "Belum Lns"',
		),
		array(
			'header'=>'S. Komplain',
			'type'=>'raw',
			'value'=>'($data->prg_komplain == "1") ? "Komplain": "Tidak Komp"',
		),
		array(
			'header'=>'S. ST',
			'type'=>'raw',
			'value'=>'($data->status_st == "1") ? "Sudah ST": "Blm-ST"',
		),
		array(
			'header'=>'SF',
			'type'=>'raw',
			'value'=>'($data->sts_salesforce == "1") ? "Masuk-SF": "Blm-SF"',
		),
		// {delete} &nbsp;
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {sent_blunas}',
			'buttons' => array(
				'sent_blunas' => array(
					    'label'=>'<i class="fa fa-paper-plane"></i>', 
					    'url'=>'Yii::app()->createUrl("SystemLogin/appointment/email_notif", array("unit_id"=>$data->unit_id))',
					    'options'=>array(),
					    'visible'=>'$data->st_lunas == 0',
					)
			),
		),
	),
)); ?>

<?php else: ?>
	
	<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'appointment-grid',
	'dataProvider'=>$model->search(),
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'htmlOptions'=> array('class'=>'custom_table'),
	'rowCssClassExpression' => 'Appointment::model()->getLegend_Listing($data)',
	'columns'=>array(
		array(
			'header'=>'Date Schedule',
			'type'=>'raw',
			'value'=>'date("d/m/Y H:i", strtotime($data->start_datetime) )',
		),
		// 'unit_id',
		array(
			'header'=>'Pemilik',
			'type'=>'raw',
			'value'=>'UnitMaster::model()->findByPk($data->unit_id)->nama_pemilik',
		),
		array(
			'header'=>'Unit Address',
			'type'=>'raw',
			'value'=>'Appointment::model()->unitAddress($data->unit_id)',
		),
		array(
			'header'=>'Cluster',
			'type'=>'raw',
			'value'=>'UnitMaster::model()->findByPk($data->unit_id)->project',
		),
		array(
			'header'=>'Bsc',
			'type'=>'raw',
			'value'=>'UnitMaster::model()->findByPk($data->unit_id)->bsc_marketing',
		),
		array(
			'header'=>'S. Lunas',
			'type'=>'raw',
			'value'=>'($data->st_lunas == "1") ? "Lunas": "Belum Lns"',
		),
		array(
			'header'=>'S. Komplain',
			'type'=>'raw',
			'value'=>'($data->prg_komplain == "1") ? "Komplain": "Tidak Komp"',
		),
		array(
			'header'=>'S. ST',
			'type'=>'raw',
			'value'=>'($data->status_st == "1") ? "Sudah ST": "Belum ST"',
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update}',
		),
	),
)); ?>


<?php endif ?>
