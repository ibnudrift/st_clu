<?php
$this->breadcrumbs=array(
	'Layanan',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Layanan',
	'subtitle'=>'Data Layanan',
);

$this->menu=array(
	array('label'=>'Add Layanan', 'icon'=>'plus-sign','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'layanan-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		'titles',
		// 'intro',
		array(
			'name'=>'Intro Deskripsi',
			'type'=>'raw',
			'value'=>'substr($data->intro, 0, 60)',
		),
		'custom_link',
		/*
		'sortings',
		'content',
		'image',
		'aktif',
		'tgl_input',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
		),
	),
)); ?>
