<?php
$this->breadcrumbs=array(
	'Bsc'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Bsc',
	'subtitle'=>'Edit Bsc',
);

$this->menu=array(
	array('label'=>'List Bsc', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Bsc', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Bsc', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>