<?php
$this->breadcrumbs=array(
	'Proyeks'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Proyek',
	'subtitle'=>'Add Proyek',
);

$this->menu=array(
	array('label'=>'List Proyek', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>