<?php

class AppointmentController extends ControllerAdmin
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layoutsAdmin/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			//'accessControl', // perform access control for CRUD operations
			array('SystemLogin.filter.AuthFilter'),
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			(!Yii::app()->user->isGuest)?
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete','index','view','create','update', 'email_notif'),
				'users'=>array(Yii::app()->user->name),
			):array(),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Appointment;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Appointment']))
		{
			$model->attributes=$_POST['Appointment'];
			if($model->validate()){
				$transaction=$model->dbConnection->beginTransaction();
				try
				{
					$model->save();
					Log::createLog("AppointmentController Create $model->id");
					Yii::app()->user->setFlash('success','Data has been inserted');
				    $transaction->commit();
					$this->redirect(array('index'));
				}
				catch(Exception $ce)
				{
				    $transaction->rollback();
				}
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Appointment']))
		{
			$model->attributes=$_POST['Appointment'];
			if($model->validate()){
				$transaction=$model->dbConnection->beginTransaction();
				try
				{
					if (intval($model->st_lunas) == 1) {
						$model->status_validasi = 1;
					}
					if (empty($model->tgl_komplain_sebelum)) {
						$model->tgl_komplain_sebelum = null;
					}
					if (empty($model->tgl_komplain_sesudah)) {
						$model->tgl_komplain_sesudah = null;
					}
					if (empty($model->tgl_komplain_selesai)) {
						$model->tgl_komplain_selesai = null;
					}
					
					$model->save();

					Log::createLog("AppointmentController Update $model->id");
					Yii::app()->user->setFlash('success','Data Edited');
				    $transaction->commit();
					$this->redirect(array('index'));
				}
				catch(Exception $ce)
				{
					echo $ce; exit;
				    $transaction->rollback();
				}
			}
		}

		$m_unit = UnitMaster::model()->findByPk($model->unit_id);

		$this->render('update',array(
			'model'=>$model,
			'm_unit'=> $m_unit,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		// if(Yii::app()->request->isPostRequest)
		// {
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			// if(!isset($_GET['ajax']))
			// isset($_POST['returnUrl']) ? $_POST['returnUrl'] :
				$this->redirect( array('index'));
		// }
		// else
			// throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Appointment('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Appointment']))
			$model->attributes=$_GET['Appointment'];

		// if ($_GET['Appointment']['date1']) {
			// 	$dates1 = date('Y-m-d H:i:s', strtotime($_GET['Appointment']['date1']));
			// 	$dates2 = date('Y-m-d H:i:s', strtotime($_GET['Appointment']['date2']));
			// 	$model->book_datetime = $dates1;
			// }

		if ($_GET['Appointment']['to_excel'] == 1){
			$this->redirect(array('downloadExcel', 
						'prg_komplain'=> $_GET['Appointment']['prg_komplain'], 
						'st_lunas'=> $_GET['Appointment']['st_lunas'], 
						'status_st'=> $_GET['Appointment']['status_st'],
						's_smarthome'=> $_GET['Appointment']['s_smarthome']
					));
		}

		$dataProvider=new CActiveDataProvider('Appointment');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
			'model'=>$model,
		));
	}

	public function actionDownloadExcel()
	{
		$n_type = $_GET['status_st'];
		$n_prg_komplain = $_GET['prg_komplain'];
		$n_st_lunas = $_GET['st_lunas'];
		$n_smarthome = $_GET['s_smarthome'];

		$criteria = new CDbCriteria;
		if (!empty($n_type)) {
			$criteria->addCondition('status_st = :nx_1');
			$criteria->params[':nx_1'] = $n_type;
		}
		if (!empty($n_prg_komplain)) {
			$criteria->addCondition('prg_komplain = :nx_2');
			$criteria->params[':nx_2'] = $n_prg_komplain;
		}
		if (!empty($n_st_lunas)) {
			$criteria->addCondition('st_lunas = :nx_3');
			$criteria->params[':nx_3'] = $n_st_lunas;
		}
		if (!empty($n_smarthome)) {
			$criteria->addCondition('s_smarthome = :n_smart');
			$criteria->params[':n_smart'] = $n_smarthome;
		}

		$model = Appointment::model()->findAll($criteria);

		header("Content-type: application/vnd-ms-excel");
		header("Content-Disposition: attachment; filename=Data_appoint.xls");
		$str = '<center><h1>Report Excel Data</h1></center>
			<table border="1">
				<tr>
					<th>KTP No</th>
					<th>Bulan ST</th>
					<th>Schedule Date</th>
					<th>Schedule Time</th>
					<th>Pemilik</th>
					<th>Unit Address</th>
					<th>Cluster</th>
					<th>Phone</th>
					<th>Bsc</th>
					<th>S. Lunas</th>
					<th>S. Komplain</th>
					<th>S. ST</th>
					<th>SF</th>
					<th>Smart Home</th>
					<th>Smart Home Tgl</th>
				</tr>';

		foreach ($model as $key => $data) {
			$str .= '<tr>';
			
			$str .= '<td>'. UnitMaster::model()->findByPk($data->unit_id)->ktp_no .'</td>';
			$str .= '<td>'. UnitMaster::model()->findByPk($data->unit_id)->bulan_st .'</td>';
			$str .= '<td>'. date("d-m-Y", strtotime($data->start_datetime) ) .'</td>';
			$str .= '<td>'. date("H:i", strtotime($data->start_datetime) ) .'</td>';
			$str .= '<td>'. UnitMaster::model()->findByPk($data->unit_id)->nama_pemilik .'</td>';
			$str .= '<td>'. Appointment::model()->unitAddress($data->unit_id) .'</td>';
			$str .= '<td>'. UnitMaster::model()->findByPk($data->unit_id)->project .'</td>';
			$str .= '<td>'. UnitMaster::model()->findByPk($data->unit_id)->phone .'</td>';
			$str .= '<td>'. UnitMaster::model()->findByPk($data->unit_id)->bsc_marketing .'</td>';
			
			if ($data->st_lunas == "1") {
				$str .= '<td>Lunas</td>';
			} else {
				$str .= '<td>Belum Lunas</td>';
			}

			if ($data->prg_komplain == "1") {
				$str .= '<td>Komplain</td>';
			} else {
				$str .= '<td>Tidak Komplain</td>';
			}

			if ($data->status_st == "1") {
				$str .= '<td>Sudah-ST</td>';
			} else {
				$str .= '<td>Blm-ST</td>';
			}
			if ($data->sts_salesforce == "1") {
				$str .= '<td>Masuk-SF</td>';
			} else {
				$str .= '<td>Blm-SF</td>';
			}

			if ($data->s_smarthome == "1") {
				$str .= '<td>Sudah order</td>';
			}elseif ($data->s_smarthome == "2") {
				$str .= '<td>Sudah diterima konsumen</td>';
			} else {
				$str .= '<td>Belum order</td>';
			}
			$str .= '<td>'. $data->s_smarthome_tgl .'</td>';
			$str .= '</tr>';
		}
		$str .= '</table>'; 

		echo $str;
	}

	public function actionExport_belum()
	{		
		$sql = 'SELECT tb1.* FROM `unit_master` tb1 WHERE tb1.id NOT IN (SELECT unit_id FROM `appointment`)';
		$model = Yii::app()->db->createCommand($sql)->queryAll();

		header("Content-type: application/vnd-ms-excel");
		header("Content-Disposition: attachment; filename=Data_appoint.xls");
		$str = '<center><h1>Report Excel Data</h1></center>
			<table border="1">
				<tr>
					<th>KTP No</th>
					<th>Bulan ST</th>
					<th>Pemilik</th>
					<th>Unit Address</th>
					<th>Cluster</th>
					<th>Bsc</th>
				</tr>';

		foreach ($model as $key => $data) {
			$data = (object) $data;
			$str .= '<tr>';
			
			$str .= '<td>'. $data->ktp_no .'</td>';
			$str .= '<td>'. $data->bulan_st .'</td>';
			$str .= '<td>'. $data->nama_pemilik .'</td>';
			$str .= '<td>'. Appointment::model()->unitAddress($data->id) .'</td>';
			$str .= '<td>'. $data->project .'</td>';
			$str .= '<td>'. $data->bsc_marketing .'</td>';
			
			$str .= '</tr>';
		}
		$str .= '</table>'; 

		echo $str;
	}

	public function actionEmail_notif()
	{
		$ids = intval($_GET['unit_id']);
		$data_unit = UnitMaster::model()->findByPk($ids);

		// sent email
		$messaged = $this->renderPartial('//mail/user_belumlunas', array(
			'm_unit' => $data_unit,
		), true);
		
		$config = array(
			'to'=>array($data_unit->email),
			'subject'=>'['.Yii::app()->name.'] Information Unit Citraland Utara '. ucwords(strtolower($m_unit->project)),
			'message'=>$messaged,
		);
		// print_r($config);
		// exit;
		Common::mail($config);

		Yii::app()->user->setFlash('success','Email telah terkirim');
		$this->redirect(array('index'));
	}

	public function loadModel($id)
	{
		$model=Appointment::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='appointment-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
