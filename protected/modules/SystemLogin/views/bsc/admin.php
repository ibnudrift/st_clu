<?php
$this->breadcrumbs=array(
	'Bscs'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Bsc','url'=>array('index')),
	array('label'=>'Add Bsc','url'=>array('create')),
);
?>

<h1>Manage Bscs</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'bsc-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'nickname',
		'nama_bsc',
		'phone',
		'email',
		'created_at',
		/*
		'updated_at',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
