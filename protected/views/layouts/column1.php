<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

	<div class="wrapper">
		<?php echo $this->renderPartial('//layouts/_header', array()); ?>

		<?php echo $content ?>

	<?php echo $this->renderPartial('//layouts/_footer', array()); ?>
	</div>
	
<?php $this->endContent(); ?>