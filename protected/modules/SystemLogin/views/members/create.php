<?php
$this->breadcrumbs=array(
	'Member Appointment'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Member Appointment',
	'subtitle'=>'Add Member Appointment',
);

$this->menu=array(
	array('label'=>'List Member Appointment', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>