<?php
$this->breadcrumbs=array(
	'Perawat',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Perawat',
	'subtitle'=>'Data Perawat',
);

$this->menu=array(
	array('label'=>'Add Perawat', 'icon'=>'plus-sign','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>
<!-- <h1>Perawat</h1> -->
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'perawat-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		'nama',
		// 'image',
		'jenis_kelamin',
		'agama',
		'pendidikan',
		'tinggi_badan',
		// 'penempatan',
		// 'durasi_kontrak_max',
		array(
			'header'=>'Durasi Waktu',
			'type'=>'raw',
			'value'=> 'Perawat::model()->getsubject_durasi($data->durasi_kontrak_max)',
		),

		/*
		'berat_badan',
		'pengalaman',
		'sertifikat',
		'provinsi',
		'kota',
		'tipe_perawat_id',
		'aktif',
		'tgl_input',
		'pengalaman_lama',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
		),
	),
)); ?>
