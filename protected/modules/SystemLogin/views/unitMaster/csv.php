<?php
$this->breadcrumbs=array(
	'Unit Master'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-life-ring',
	'title'=>'Unit Master',
	'subtitle'=>'Add Unit Master',
);

$this->menu=array(
	// array('label'=>'List Unit Master', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'input-product-form',
    // 'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<?php echo $form->errorSummary($model); ?>
<div class="row-fluid">
	<div class="span8">
		<?php if(Yii::app()->user->hasFlash('success')): ?>
		    <?php $this->widget('bootstrap.widgets.TbAlert', array(
		        'alerts'=>array('success'),
		    )); ?>
		<?php endif; ?>
		<!-- ----------------- Action ----------------- -->
		<div class="widgetbox block-rightcontent">                        
		    <div class="headtitle">
		        <h4 class="widgettitle">Upload File CSV</h4>
		    </div>
		    <div class="widgetcontent">
				<?php echo $form->fileFieldRow($model,'file',array(
				'hint'=>'<b>Note:</b> Upload file CSV di sini', 'style'=>"width: 100%")); ?>

				<!-- <p>Contoh file excel download <a href="<?php echo Yii::app()->baseUrl ?>/images/template-excel.xlsx">di sini</a></p>
				<p>Save file excel sebagai file CSV dan upload di sini</p> -->

		    </div>
		</div>
	</div>
	<div class="span4">
		<!-- ----------------- Action ----------------- -->
		<div class="widgetbox block-rightcontent">                        
		    <div class="headtitle">
		        <h4 class="widgettitle">Action</h4>
		    </div>
		    <div class="widgetcontent">

				<?php $this->widget('bootstrap.widgets.TbButton', array(
					'buttonType'=>'submit',
					'type'=>'primary',
					'label'=>'Upload CSV',
					'htmlOptions'=>array('class'=>'btn-large', 'value'=>'submit', 'name'=>'submit'),
				)); ?>
				<?php $this->widget('bootstrap.widgets.TbButton', array(
					// 'buttonType'=>'submit',
					// 'type'=>'info',
					'url'=>CHtml::normalizeUrl(array('index')),
					'label'=>'Cancel',
					'htmlOptions'=>array('class'=>'btn-large'),
				)); ?>
		    </div>
		</div>

	</div>
</div>

<?php $this->endWidget(); ?>

