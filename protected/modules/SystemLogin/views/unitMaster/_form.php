<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'unit-master-form',
    'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="widget">
<h4 class="widgettitle">Data Master Unit</h4>
<div class="widgetcontent">

	<?php if ($model->scenario == 'insert'): ?>
		<?php echo $form->textFieldRow($model,'blok',array('class'=>'span5', )); ?>

		<?php echo $form->textFieldRow($model,'kav',array('class'=>'span5',)); ?>

		<?php echo $form->textFieldRow($model,'ktp_no',array('class'=>'span5' )); ?>

		<?php echo $form->textFieldRow($model,'nama_pemilik',array('class'=>'span5' )); ?>

		<?php echo $form->textFieldRow($model,'phone',array('class'=>'span5' )); ?>

		<?php echo $form->textFieldRow($model,'tipe_rumah',array('class'=>'span5',)); ?>

		<?php echo $form->textFieldRow($model,'bulan_st',array('class'=>'span5',)); ?>

		<?php echo $form->textFieldRow($model,'bsc_marketing',array('class'=>'span5',)); ?>

		<?php echo $form->textFieldRow($model,'bsc_phone',array('class'=>'span5',)); ?>
		
		<?php echo $form->textFieldRow($model,'project',array('class'=>'span5',)); ?>

	<?php else: ?>

		<?php echo $form->textFieldRow($model,'blok',array('class'=>'span5', 'readonly'=> 'readonly')); ?>

		<?php echo $form->textFieldRow($model,'kav',array('class'=>'span5','readonly'=> 'readonly')); ?>

		<?php echo $form->textFieldRow($model,'ktp_no',array('class'=>'span5' )); ?>

		<?php echo $form->textFieldRow($model,'nama_pemilik',array('class'=>'span5' )); ?>

		<?php echo $form->textFieldRow($model,'phone',array('class'=>'span5' )); ?>

		<?php echo $form->textFieldRow($model,'tipe_rumah',array('class'=>'span5','readonly'=> 'readonly')); ?>

		<?php echo $form->textFieldRow($model,'bulan_st',array('class'=>'span5','readonly'=> 'readonly')); ?>

		<?php echo $form->textFieldRow($model,'bsc_marketing',array('class'=>'span5','readonly'=> 'readonly')); ?>

		<?php echo $form->textFieldRow($model,'bsc_phone',array('class'=>'span5','readonly'=> 'readonly')); ?>
		
		<?php echo $form->textFieldRow($model,'project',array('class'=>'span5','readonly'=> 'readonly')); ?>

	<?php endif ?>


		<?php 
		if ($model->scenario == 'insert') {
			$this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? 'Add' : 'Save',
			)); 
		}
		?>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			// 'buttonType'=>'submit',
			// 'type'=>'info',
			'url'=>CHtml::normalizeUrl(array('index')),
			'label'=>'Batal',
		)); ?>
</div>
</div>
<div class="alert">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Warning!</strong> Fields with <span class="required">*</span> are required.
</div>

<?php $this->endWidget(); ?>
