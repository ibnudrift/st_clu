<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('member_id')); ?>:</b>
	<?php echo CHtml::encode($data->member_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ktp_no')); ?>:</b>
	<?php echo CHtml::encode($data->ktp_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('blok')); ?>:</b>
	<?php echo CHtml::encode($data->blok); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kav')); ?>:</b>
	<?php echo CHtml::encode($data->kav); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_pemilik')); ?>:</b>
	<?php echo CHtml::encode($data->nama_pemilik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipe_rumah')); ?>:</b>
	<?php echo CHtml::encode($data->tipe_rumah); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('jadwal_st_kontraktor')); ?>:</b>
	<?php echo CHtml::encode($data->jadwal_st_kontraktor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jadwal_st_web')); ?>:</b>
	<?php echo CHtml::encode($data->jadwal_st_web); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bsc_marketing')); ?>:</b>
	<?php echo CHtml::encode($data->bsc_marketing); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_at')); ?>:</b>
	<?php echo CHtml::encode($data->created_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_at')); ?>:</b>
	<?php echo CHtml::encode($data->updated_at); ?>
	<br />

	*/ ?>

</div>