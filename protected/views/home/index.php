
  <!-- Masthead -->
  <header class="masthead text-white text-center p-0">
    <div class="overlay"></div>
    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel" data-interval="3500">
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img class="d-block w-100" src="<?php echo $this->assetBaseurl ?>slide-1.jpg" alt="First slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="<?php echo $this->assetBaseurl ?>slide-2.jpg" alt="Second slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="<?php echo $this->assetBaseurl ?>slide-3.jpg" alt="Third slide">
        </div>
      </div>
    </div>
  </header>

  <!-- Icons Grid -->
  <section class="features-icons bg-light text-center">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
            <h3>Sebuah kawasan hunian terpadu yang eksklusif, hadir di Surabaya.</h3>
            <p class="lead mb-0">Satu diantara karya Group Ciputra menghadirkan kawasan hunian terpadu yang eksklusif, yang modern, green dan smart dengan fasilitas lengkap untuk memenuhi segala kebutuhan keluarga.</p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Image Showcases -->
  <section class="showcase">
    <div class="container-fluid p-0">
      <div class="row no-gutters">

        <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image: url('<?php echo $this->assetBaseurl ?>banner-1.jpg');"></div>
        <div class="col-lg-6 order-lg-1 my-auto showcase-text">
          <h2>CitraLand Utara Surabaya</h2>
          <div class="py-1"></div>
          <p class="lead mb-0">mengusung konsep kawasan dengan design yang modern, elegan dengan ruang terbuka hijau (RTH) serta design yang fungsional. Dikembangkan di atas lahan seluas 500 ha, hadir sebagai kawasan hunian terpadu. Lengkap dengan berbagai fasilitas seperti clubhouse, fasilitas komersial, edukasi dan tempat ibadah untuk melengkapi ruang hidup anda.</p>
        </div>
      </div>
      <div class="row no-gutters">
        <div class="col-lg-6 text-white showcase-img" style="background-image: url('<?php echo $this->assetBaseurl ?>banner-2.jpg');"></div>
        <div class="col-lg-6 my-auto showcase-text">
          <h2>Cluster yang telah hadir diantaranya </h2>
          <div class="py-1"></div>
          <p class="lead mb-0">Pelican Hill, Green Hill, Palma Classica, Palma Grandia, Bukit Palma dan yang paling baru yaitu kawasan Northwest yang dilauncing sejak lima tahun terakhir sejak 2015 yaitu Northwest Park, Northwest Lake, Northwest Hill, Northwest Central dan Northwest Boulevard, selalu menjadi pilihan favorit keluarga.</p>
        </div>
      </div>
    </div>
  </section>

  <!-- Testimonials -->
  <section id="syarat-dv" class="testimonials bg-light">
    <div class="container">
      <h2 class="mb-5 text-center">Q & A</h2>
      <div class="py-2"></div>
      <?php 
      $n_qa = [
                [
                  'question' => 'NO KTP anda belum terdaftar?',
                  'answer' => 'Jika nomer KTP tidak terdaftar. harap menghubungi 031-741 2888 (up. ALVIA/FANI/NOVI) atau email marketing@citralandutara.com',
                ],
                [
                  'question' => 'Jika KTP sudah terdaftar apa selanjutnya?',
                  'answer' => 'Masukkan nomor KTP sesuai dengan nama tercetak di SPT, tekan “Search” dan tekan “Next”, anda akan di arahkan pada form penjadwalan serah terima unit anda',
                ],
                [
                  'question' => 'Langkah - langkah penjadwalan serah terima unit',
                  'answer' => 'Klik tombol &quot;SERAH TERIMA&quot;, masukkan nomor KTP, tekan “Search” dan tekan “Next”, anda akan diarahkan ke form penjadwalan <br>-&gt; unit rumah/ruko anda tampil <br>-&gt; pilih tanggal dan waktu yang tersedia <br>-&gt; masukkan nama yg akan hadir dan email <br>-&gt; identitas dan detail unit rumah/ruko anda akan tampil <br>-&gt; konfirmasi <br>-&gt; detail lengkap dan penjadwalan serah terima yg dipilih akan diemail.',
                ],
                [
                  'question' => 'Tidak bisa pilih tanggal dan waktu sesuai keinginan saya',
                  'answer' => 'Tanggal dan waktu tersebut sudah penuh (&quot;maksimal 5 unit/jam&quot;), silahkan pilih tanggal dan waktu yang tersedia.',
                ],
                [
                  'question' => 'Bagaimana jika pemilik unit berhalangan hadir?',
                  'answer' => 'Silahkan buat reschedule pada sistem kami H-2 sebelum janji yang telah anda inputkan atau dapat diwakilkan dengan mengupload surat kuasa.',
                ],
                [
                  'question' => 'Pertanyaan lainnya?',
                  'answer' => 'Harap menghubungi 031-741 2888 (up. ALVIA/FANI/NOVI) atau WA. +62 821 3173 1536.',
                ],
                                
              ];
      ?>
      <div id="accordion" class="customs_qa">
        <?php foreach ($n_qa as $key => $value): ?>
        <div class="card mb-3">
          <div class="card-header" id="heading_<?php echo $key ?>">
            <h5 class="mb-0">
              <button class="btn btn-link" data-toggle="collapse" data-target="#collapse_<?php echo $key ?>" aria-expanded="true" aria-controls="collapse_<?php echo $key ?>">
                <?php echo $value['question'] ?>
              </button>
            </h5>
          </div>

          <div id="collapse_<?php echo $key ?>" class="collapse <?php echo ($key == 0)? "show":"" ?>" aria-labelledby="heading_<?php echo $key ?>" data-parent="#accordion">
            <div class="card-body">
              <p><?php echo $value['answer'] ?></p>
            </div>
          </div>
        </div>
        <?php endforeach ?>
      </div>

      <div class="py-2"></div>
      <div class="clear"></div>
    </div>
  </section>

  <!-- Call to Action -->
  <section id="contact-us" class="call-to-action text-black">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-xl-9 mx-auto text-center">
          <h2 class="mb-4">Contact Us</h2>
        </div>
        <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
          <div class="py-3"></div>
          <div class="row">
            <div class="col-md-5">
              <address>
                <a target="_blank" href="https://goo.gl/maps/Wyv6Nds41PdaM92C8"><i class="fa fa-map-marker"></i> &nbsp;Northwest - CitraLand Surabaya</a>
              </address>
              <address>
                <i class="fa fa-envelope"></i> &nbsp;marketing@citralandutara.com
              </address>
              <address>
                <i class="fa fa-phone"></i> &nbsp;Phone : +62 31 741 2888
              </address>
              <address>
                <i class="fa fa-whatsapp"></i> &nbsp;WA : 0821 3173 1536
              </address>
              
            </div>
            <div class="col-md-7">
              <!-- contact form -->
              <form>
                <div class="form-group">
                  <input type="text" name="name" class="form-control" placeholder="Name">
                </div>
                <div class="form-group">
                  <input type="email" name="email" class="form-control" placeholder="Email">
                </div>
                <div class="form-group">
                  <input type="text" name="subject" class="form-control" placeholder="Subject">
                </div>
                <div class="form-group">
                  <textarea name="message" rows="4" class="form-control" placeholder="Messages"></textarea>
                </div>
                
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>

              <!-- end contact form -->
            </div>
          </div>

        </div>
      </div>
    </div>
  </section>

<style type="text/css">
  address a{
    color: #000;
    text-decoration: none;
  }
</style>