<?php
$this->breadcrumbs=array(
	'Clusters'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Cluster',
	'subtitle'=>'Edit Cluster',
);

$this->menu=array(
	array('label'=>'List Cluster', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Cluster', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Cluster', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>