<div class="outer-wrapper-boxed">
	<div class="middles_content boxed_registers">
		
		<div class="container-fluid" id="grad1">
		    <div class="row justify-content-center mt-0">
		        <div class="col-11 col-sm-9 col-md-9 col-lg-7 text-center my-5">
		            <div class="card px-0 py-4 my-3">

		            	<h2><strong>Reschedule unit Handover</strong></h2>
		            	<!-- Start content -->
		            	<div class="py-2"></div>
		            	<div class="form-card rescheules">
                            <div class="row justify-content-center">
                                <div class="col-md-9 col-12 text-center blocks_outers_infosuccess">
                                    <div class="box-fix-active_appointmentf">
                                        <h4><b>Appointment Schedule</b></h4>
                                        <div class="py-2"></div>

                                        <?php if(Yii::app()->user->hasFlash('danger')): ?>
		                                    <?php $this->widget('bootstrap.widgets.TbAlert', array(
		                                        'alerts'=>array('danger'),
		                                        'fade'=>false,
		                                    )); ?>
		                                <?php endif; ?>

                                		<?php if(Yii::app()->user->hasFlash('success')): ?>
		                                    <?php $this->widget('bootstrap.widgets.TbAlert', array(
		                                        'alerts'=>array('success'),
		                                        'fade'=>false,
		                                    )); ?>
		                                <?php endif; ?>
		                                
                                        <div class="row text-left">
                                        	<div class="col-12 col-md-6">
                                        		<p>Project: <br><b>Citraland Utara Surabaya</b><br>
                                        		Cluster: <br><b><?php echo $data_unit->project ?></b><br>
                                        		<?php 
                                        			$n_date =  date('Y-m-d', strtotime($model->start_datetime)); 
                                        			$ns_time = date('H:i', strtotime($model->start_datetime));
                                        			$arr_time = [
                                        						'08:00',
																'09:00',
																'10:00',
																'11:00',
																'13:00',
																'14:00',
																'15:00',
																'16:00',
                                        						];
                                        		?></p>
                                        		<form action="" method="post" class="b_form_reschedule">
                                        			<div class="form-group">
				                                    	<label>Date</label>
				                                    	<input type="text" name="Appointment[book_datetime]" required="required" class="set_cdate datepicker_def form-control" value="<?php echo $n_date ?>">
				                                    </div>
				                                    <div class="form-group">
				                                    	<label>Time</label>
				                                    	<select name="Appointment[start_datetime]" required="required" class="timepicker_def form-control" id="">
				                                    		<option value="">Choose Time</option>
				                                    		<?php foreach ($arr_time as $ke_t => $val_t): ?>
				                                    		<option <?php if ($ns_time == $val_t): ?>selected="selected"<?php endif ?> value="<?php echo $val_t ?>"><?php echo $val_t ?></option>
				                                    		<?php endforeach ?>
				                                    	</select>
				                                    </div>
				                                    <?php if ($st_form): ?>
				                                    <div class="form-group">
				                                    	<input type="hidden" name="Appointment[reschedules]" value="1">
				                                    	<button type="button" class="confirm_changes btn btn-primary">Update</button>
				                                    </div>
				                                    <?php endif ?>
                                        		</form>
                                        	</div>
                                        	<div class="col-12 col-md-6">
                                        		<div class="information_user">
                                        			<p>Name:<br>
                                        			<b><?php echo $data_unit->nama_pemilik ?></b> <br>
                                        				<?php if (isset($model->kuasa_file) && isset($model->kuasa_nama)): ?>
                                        				Nama yg Hadir: <?php echo $model->kuasa_nama ?><br>
                                        				Phone: <?php echo $model->phone2 ?><br>
                                        				Email: <?php echo $model->email ?><br><br>
                                        				<?php endif ?>

                                        				<?php if (empty($model->kuasa_nama)): ?>
                                        				Phone: <?php echo $data_unit->phone ?>&nbsp;<br>
                                        				Email: <?php echo $model->email ?><br>
                                        				<?php endif ?>
                                        				Address:&nbsp;<?php echo $data_unit->blok.' / '.$data_unit->kav.' Type '. $data_unit->tipe_rumah ?><br>
                                        				City: Surabaya
                                        				<br>BSC Name: <?php echo $data_unit->bsc_marketing ?><br>
                                        				BSC Phone: <?php echo $data_unit->bsc_phone ?>
                                        			</p>
                                        		</div>
                                        	</div>
                                        </div>
                                        <div class="py-2"></div>
                                        <div class="alert alert-info pn_infos2">
                                        	<p class="m-0">*) Note: Klik update jika anda ingin mengubah jadwal. Perubahan jadwal maksimal hanya 1x.</p>
                                        </div>
                                    	<div class="clear"></div>
                                    </div>

                                    <div class="celar"></div>
                                </div>
                            </div>
                        </div>

		            	<!-- End content -->
		            	<div class="clear"></div>
		            </div>
		        </div>
		    </div>
		</div>

	</div>
</div>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/themes/base/theme.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<script type="text/javascript">
	$('.confirm_changes').on('click', function(){
		if (confirm("Are you sure to Change Schedule?") == true) {
			$('form.b_form_reschedule').submit();
		  } else {
		    return false;
		  }
	});

	if (parseInt(<?php echo $bulan_no ?>) != 12) {
		var maxDndate = addMonths(new Date("<?php echo $data_unit->bulan_st ?>"),1);
	}else{
		var maxDndate = new Date("<?php echo $data_unit->bulan_st ?>");
	}

	$('.datepicker_def').datepicker({
		// 'minDate': "+7d",
		'minDate': new Date("<?php echo $model->book_datetime ?>"),
		'dateFormat': "yy-mm-dd",
		'maxDate': maxDndate,
		beforeShowDay: function(date) {
	        var day = date.getDay();
	        return [(day != 0), ''];
	    }
	});

	function addMonths(date, months) {
	    var d = date.getDate();
	    date.setMonth(date.getMonth() + +months);
	    if (date.getDate() != d) {
	      date.setDate(0);
	    }
	    return date;
	}

	<?php if (!$st_form): ?>
		 $("input.set_cdate").datepicker( "option", "disabled", true );
		$('select, input').attr('readonly', 'readonly');
	<?php endif; ?>
</script>