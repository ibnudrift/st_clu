<?php

/**
 * This is the model class for table "tb_testimonial".
 *
 * The followings are the available columns in table 'tb_testimonial':
 * @property integer $id
 * @property string $nama
 * @property string $content
 * @property string $image
 * @property integer $aktif
 * @property string $date_input
 * @property integer $sortings
 */
class Testimonial extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Testimonial the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_testimonial';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama, content', 'required'),
			array('aktif, sortings', 'numerical', 'integerOnly'=>true),
			array('nama, image', 'length', 'max'=>225),
			array('date_input', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nama, content, image, aktif, date_input, sortings', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Nama',
			'content' => 'Content',
			'image' => 'Image',
			'aktif' => 'Aktif',
			'date_input' => 'Date Input',
			'sortings' => 'Sortings',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('aktif',$this->aktif);
		$criteria->compare('date_input',$this->date_input,true);
		$criteria->compare('sortings',$this->sortings);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}