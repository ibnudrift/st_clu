<?php
$this->breadcrumbs=array(
	'Proyeks'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Proyek',
	'subtitle'=>'Edit Proyek',
);

$this->menu=array(
	array('label'=>'List Proyek', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Proyek', 'icon'=>'plus-sign','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>