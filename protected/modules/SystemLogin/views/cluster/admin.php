<?php
$this->breadcrumbs=array(
	'Clusters'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Cluster','url'=>array('index')),
	array('label'=>'Add Cluster','url'=>array('create')),
);
?>

<h1>Manage Clusters</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'cluster-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'nama',
		'proyek_id',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
