<?php

class PerawatController extends ControllerAdmin
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layoutsAdmin/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			//'accessControl', // perform access control for CRUD operations
			array('SystemLogin.filter.AuthFilter'),
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			(!Yii::app()->user->isGuest)?
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete','index','view','create','update'),
				'users'=>array(Yii::app()->user->name),
			):array(),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Perawat;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Perawat']))
		{
			$model->attributes=$_POST['Perawat'];

			$image = CUploadedFile::getInstance($model,'image');
			if ($image->name != '') {
				$model->image = substr(md5(time()),0,5).'-'.$image->name;
			}

			if($model->validate()){
				$transaction=$model->dbConnection->beginTransaction();
				try
				{
					$model->tgl_input = date("Y-m-d H:i:s");

					if ($image->name != '') {
						$image->saveAs(Yii::getPathOfAlias('webroot').'/images/intern_perawat/'.$model->image);
					}

					$model->durasi_kontrak_max = serialize($model->durasi_kontrak_max);
					$model->tipe_perawat_id = serialize($model->tipe_perawat_id);

					$model->save();

					Log::createLog("PerawatController Create $model->id");
					Yii::app()->user->setFlash('success','Data has been inserted');
				    $transaction->commit();
					$this->redirect(array('index'));
				}
				catch(Exception $ce)
				{
				    $transaction->rollback();
				}
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Perawat']))
		{
			$image = $model->image;//mengamankan nama file
			$model->attributes=$_POST['Perawat'];
			$model->image = $image;//mengembalikan nama file

			$image = CUploadedFile::getInstance($model,'image');
			if ($image->name != '') {
				$model->image = substr(md5(time()),0,5).'-'.$image->name;
			}

			if($model->validate()){
				$transaction=$model->dbConnection->beginTransaction();
				try
				{
					if ($image->name != '') {
						$image->saveAs(Yii::getPathOfAlias('webroot').'/images/intern_perawat/'.$model->image);
					}

					// save duration
					// $str_durasi = '';
					// if (is_array($model->durasi_kontrak_max) && count($model->durasi_kontrak_max) > 0) {
					// 	foreach ($model->durasi_kontrak_max as $key => $value) {
					// 		$str_durasi .= $value."||";
					// 	}
					// }
					// $model->durasi_kontrak_max = substr($str_durasi, 0, -2);
					$model->durasi_kontrak_max = serialize($model->durasi_kontrak_max);

					// save category_id
					// $str_category = '';
					// if (is_array($model->tipe_perawat_id) && count($model->tipe_perawat_id) > 0) {
					// 	foreach ($model->tipe_perawat_id as $key => $value) {
					// 		$str_category .= $value."||";
					// 	}
					// }
					// $model->tipe_perawat_id = substr($str_category, 0, -2);
					$model->tipe_perawat_id = serialize($model->tipe_perawat_id);

					$model->save();

					Log::createLog("PerawatController Update $model->id");
					Yii::app()->user->setFlash('success','Data Edited');
				    $transaction->commit();
					$this->redirect(array('index'));
				}
				catch(Exception $ce)
				{
				    $transaction->rollback();
				}
			}
		}


		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		// if(Yii::app()->request->isPostRequest)
		// {
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			// if(!isset($_GET['ajax']))
				$this->redirect( array('index'));
		// }
		// else
		// 	throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Perawat('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Perawat']))
			$model->attributes=$_GET['Perawat'];

		$dataProvider=new CActiveDataProvider('Perawat');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Perawat::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='perawat-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


	public function actionListprovinsi()
	{
		// get province
		$criteria=new CDbCriteria;
		$criteria->group = 'province_id';
		$data = City::model()->findAll($criteria);

		$datas = array();
		foreach ($data as $key => $value) {
			$datas[$value->province] = $value->province;
		}

		return $datas;
	}
	
	public function actionListcity()
	{
		$province_id = $_POST['province_name'];
		// get province
		$criteria=new CDbCriteria;
		$criteria->addCondition('t.province = :prov_name');
		$criteria->params[':prov_name'] = $province_id;
		$data = City::model()->findAll($criteria);

		$datas = array();
		foreach ($data as $key => $value) {
			$datas[$value->city_name] = $value->city_name;
		}

		return $datas;
	}

	public function actionGetTo()
	{
		$to = City::model()->findAll('`province_id` = :province_id', array(':province_id'=>$_POST['province']));
		// $datas = array();
		$str = '<option value="">--- Pilih Kota ---</option>';
		foreach ($to as $value) {
			$str .= '<option value="'.$value->id.'">'.$value->type.' '.$value->city_name.'</option>';
		}
		echo($str);
	}

}
