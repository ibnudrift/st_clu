<?php

/**
 * This is the model class for table "appointment".
 *
 * The followings are the available columns in table 'appointment':
 * @property string $id
 * @property string $transaction_no
 * @property string $book_datetime
 * @property string $start_datetime
 * @property string $end_datetime
 * @property string $notes
 * @property integer $status
 * @property integer $unit_id
 * @property integer $user_id
 * @property integer $reschedules
 * @property integer $status_st
 * @property integer $prg_komplain
 * @property string $created_at
 * @property string $updated_at
 */
class Appointment extends CActiveRecord
{
	public $info_unit, $date1, $date2, $to_excel, $bsc_name, $bsc_phone, $bulan_st;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Appointment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'appointment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status_validasi, unit_id, user_id, status_st, prg_komplain', 'numerical', 'integerOnly'=>true),
			array('transaction_no', 'length', 'max'=>255),
			array('book_datetime, start_datetime, end_datetime, notes, proyek_id, created_at, updated_at, kuasa_file, kuasa_nama, kuasa_phone, email, st_lunas, phone2, reschedules, tgl_st_lunas, tgl_set_st, sts_komplain_sebelum, tgl_komplain_sebelum, note_komplain_sebelum, sts_komplain_sesudah, tgl_komplain_sesudah, note_komplain_sesudah, sts_komplain_selesai, tgl_komplain_selesai, note_komplain_selesai, sts_salesforce, s_smarthome, s_smarthome_tgl', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, transaction_no, book_datetime, start_datetime, end_datetime, notes, status_validasi, unit_id, user_id, reschedules, status_st, prg_komplain, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'No.',
			'transaction_no' => 'Transaction No',
			'book_datetime' => 'Book Datetime',
			'start_datetime' => 'Date Schedule',
			'end_datetime' => 'End Datetime',
			'notes' => 'Notes / Comments',
			'status' => 'Status',
			'unit_id' => 'Unit',
			'user_id' => 'User',
			'reschedules' => 'Reschedules',
			'status_validasi' => 'Status Validasi',
			'status_st' => 'Pilih Status ST',
			'prg_komplain' => 'Status Komplain',
			'st_lunas' => 'Status Lunas',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'date1' => 'Tanggal Awal',
			'date2' => 'Tanggal Akhir',
			'tgl_st_lunas' => 'Tanggal Lunas',
			'bulan_st' => 'Bulan ST',
			'tgl_set_st' => 'Tanggal ST',
			'sts_salesforce' => 'Status Salesforce',

			'sts_komplain_sebelum'=>'Sts Komplain Sebelum ST',
			'tgl_komplain_sebelum'=>'Tgl Komplain Sebelum ST',
			'note_komplain_sebelum'=>'Note Komplain Sebelum ST',
			'sts_komplain_sesudah'=>'Sts Komplain Sesudah ST',
			'tgl_komplain_sesudah'=>'Tgl Komplain Sesudah ST',
			'note_komplain_sesudah'=>'Note Komplain Sesudah ST',
			's_smarthome'=>'Status SmartHome',
			's_smarthome_tgl'=>'SmartHome Tgl Terima',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;


		$criteria->compare('id',$this->id,true);
		$criteria->compare('transaction_no',$this->transaction_no,true);
		$criteria->compare('book_datetime',$this->book_datetime,true);
		$criteria->compare('start_datetime',$this->start_datetime,true);
		$criteria->compare('end_datetime',$this->end_datetime,true);
		$criteria->compare('notes',$this->notes,true);
		$criteria->compare('status_validasi',$this->status_validasi);
		$criteria->compare('unit_id',$this->unit_id);
		$criteria->compare('proyek_id',$this->proyek_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('reschedules',$this->reschedules);
		$criteria->compare('status_st',$this->status_st,true);
		$criteria->compare('prg_komplain',$this->prg_komplain,true);
		$criteria->compare('st_lunas',$this->st_lunas,true);

		$criteria->order = 't.start_datetime ASC';	
		
		// if ($_GET['Appointment']['date1']) {
		// 	$dates1 = date('Y-m-d H:i:s', strtotime($_GET['Appointment']['date1']));
		// 	$dates2 = date('Y-m-d H:i:s', strtotime($_GET['Appointment']['date2']));

		// 	$criteria->addCondition('book_datetime >= :date1');
		// 	$criteria->addCondition('book_datetime < :date2');
		// 	$criteria->params = array(':date1'=>$dates1, ':date2'=> $dates2);
		// }

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => false,
		));
	}

	public function getLegend_Listing($data)
	{
		$color = '';

		if ($data->st_lunas == 0 && intval($data->status_validasi) == 0) {
			$color = 'c_white';
		}elseif ($data->st_lunas == 1 && intval($data->status_validasi) == 1 && $data->status_st == 0) {
			$color = 'c_blue';
		}
		// LUNAS
		elseif ($data->status_st == 1 && $data->status_validasi == 1) {
			$color = 'c_green';
		}

		// elseif (intval($data->status) == 2) {
			// $color = 'c_red';
		// }else{
		// 	$color = 'c_grey';
		// }

		return $color;
	}

	public function unitAddress($unit_id)
	{
		$data_unit = UnitMaster::model()->findByPk($unit_id);
		$str = $data_unit->blok . ' '. $data_unit->kav . ' - Type '. ucwords(strtolower($data_unit->tipe_rumah));
		return $str;		
	}

}