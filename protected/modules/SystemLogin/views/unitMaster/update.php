<?php
$this->breadcrumbs=array(
	'Unit Master'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Master Unit',
	'subtitle'=>'Edit Master Unit',
);

$this->menu=array(
	array('label'=>'List Master Unit', 'icon'=>'th-list','url'=>array('index')),
	// array('label'=>'Add Master Unit', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Master Unit', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>