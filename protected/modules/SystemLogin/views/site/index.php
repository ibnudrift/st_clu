<?php
$session = new CHttpSession;
$session->open();
$checks_user = $session['login']['group_id'];

$this->breadcrumbs=array(
    'Dashboard',
);
?>
    
<div class="pageheader">
    
    <div class="pageicon"><span class="fa fa-laptop"></span></div>
    <div class="pagetitle">
        <h5>All Features Summary</h5>
        <h1>Dashboard</h1>
    </div>
</div><!--pageheader-->

<div class="maincontent">
    <div class="maincontentinner">
        <div class="row-fluid">
            <div id="dashboard-left" class="span8">
                <h5 class="subtitle">Menu</h5>

                <ul class="shortcuts">
                    <?php if ($checks_user != 8): ?>
                    <li class="products">
                        <a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/unitMaster/index')); ?>">
                            <i class="icon-cms fa fa-folder"></i>
                            <span class="shortcuts-label">Unit Master</span>
                        </a>
                    </li>
                    <?php else: ?>
                        <li class="products">
                        <a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/appointment/index')); ?>">
                            <i class="icon-cms fa fa-folder"></i>
                            <span class="shortcuts-label">Appointment</span>
                        </a>
                    </li>
                    <?php endif; ?>
                </ul>
            </div> <!-- span-8 -->
            
            <div id="dashboard-right" class="span4">
                
                <h5 class="subtitle">INFORMATION</h5>
                <div class="divider15"></div>

                <div class="alert alert-block">
                      <button data-dismiss="alert" class="close" type="button">&times;</button>
                      <h4>Documentation!</h4>
                      <p style="margin:5px 0">View Documentation Guide 
                        <a target="_blank" href="<?php echo Yii::app()->baseUrl.'/images/' ?>">Click here</a></p>
                </div>
                <br />
                                        
            </div><!--span4-->
        </div><!--row-fluid-->
        
        <div class="footer">
            <div class="footer-left">
                <span>Copyright &copy; <?php echo date('Y'); ?> by <?php echo Yii::app()->name ?>.</span>
            </div>
            <div class="footer-right">
                <span>All Rights Reserved.</span>
                <span class="hide hidden" style="opacity: 0;">developed by <a target="_blank" href="http://www.atechnoweb.com">www.atechnoweb.com</a></span>
            </div>
        </div><!--footer-->
        
    </div><!--maincontentinner-->
</div><!--maincontent-->