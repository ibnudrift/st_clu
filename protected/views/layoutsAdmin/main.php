<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/asset/backend/css/style.default.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/asset/backend/css/styles.css" type="text/css" />

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/asset/backend/css/responsive-tables.css">
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/asset/backend/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/asset/backend/js/jquery-migrate-1.1.1.min.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/asset/backend/js/jquery-ui-1.10.3.min.js"></script>
    
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/asset/backend/js/modernizr.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/asset/backend/js/bootstrap.min.js"></script>
    
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/asset/backend/js/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/asset/backend/js/responsive-tables.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/asset/backend/js/jquery.slimscroll.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/asset/backend/js/custom.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/asset/backend/js/my.js"></script>
    <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="<?php // echo Yii::app()->baseUrl; ?>/asset/backend/js/excanvas.min.js"></script><![endif]-->


</head>
<body>
<style type="text/css">
    .header .logo a{
        display: inline-block;
        margin: -12px auto 0;
        max-width: 145px;
        padding-top: 0px;
    }
    .header .logo{
        padding-bottom: 12px; background-color: transparent; padding-top: 22px;
    }
</style>
<div id="mainwrapper" class="mainwrapper">
    <!-- // start header -->
    <div class="header">
        <div class="logo">
            <a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/site/index')); ?>">
                <img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/new/logo-head.png" alt="Content Management System - Logo" />
            </a>
        </div>
        <div class="headerinner">
            <ul class="headmenu">
            <li>
                
            </li>

                <li class="right">
                    <div class="userloggedinfo">
                        <?php /*
                        <img src="<?php echo Yii::app()->baseUrl; ?>/asset/backend/images/photos/thumb1.png" alt="" />
                        */ ?>
                        <div class="userinfo">
                            <h5><?php echo Yii::app()->user->name ?> <small>- <?php echo Yii::app()->user->name ?></small></h5>
                            <ul>
                                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/administrator/edit')); ?>"><?php echo Tt::t('admin', 'Edit Profile') ?></a></li>
                                <?php /*
                                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/user/index')); ?>"><?php echo Tt::t('admin', 'Change Password') ?></a></li>
                                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/home/logout')); ?>"><?php echo Tt::t('admin', 'Sign Out') ?></a></li>
                                */ ?>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!-- headmenu -->
        </div>
    </div>
    <!-- // end header -->
    
    <div class="leftpanel">
        <?php echo $this->renderPartial('//layoutsAdmin/left_menu'); ?>
        
    </div><!-- leftpanel -->
    
    <div class="rightpanel">
        
        <?php echo $content ?>
        
    </div><!--rightpanel-->
    
</div>

<?php /*
<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.js"></script>
*/ ?>

<link rel="stylesheet" href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<script type="text/javascript" src="//cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.11/sorting/date-eu.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.min.css">

<script type="text/javascript">
    jQuery(document).ready(function() {
        

     jQuery('.custom_table > table').DataTable({
            // "order": [[ 0, 'asc' ]],
            "columnDefs" : [{"targets":0, "type":"date-eu"}],
            "ordering" : true,
            "pageLength": 20,
        });

     jQuery('.grid-view').addClass('table-responsive');

      // simple chart
        var flash = [[0, 11], [1, 9], [2,12], [3, 8], [4, 7], [5, 3], [6, 1]];
        var html5 = [[0, 5], [1, 4], [2,4], [3, 1], [4, 9], [5, 10], [6, 13]];
      var css3 = [[0, 6], [1, 1], [2,9], [3, 12], [4, 10], [5, 12], [6, 11]];
            
        function showTooltip(x, y, contents) {
            jQuery('<div id="tooltip" class="tooltipflot">' + contents + '</div>').css( {
                position: 'absolute',
                display: 'none',
                top: y + 5,
                left: x + 5
            }).appendTo("body").fadeIn(200);
        }    
        
        //datepicker
        jQuery('#datepicker').datepicker();
        
        jQuery('.datepicker').datepicker({
            dateFormat: "yy-mm-dd",
            // minDate: 0,
        });

        jQuery('.datepicker_time').datetimepicker({
            allowTimes:[
              '08:00',
              '09:00',
              '10:00',
              '11:00',
              '13:00',
              '14:00',
              '15:00',
              '16:00',
              '14:00',
             ],
            minDate:'<?php echo date("Y/m/d"); ?>',
        });

        // tabbed widget
        jQuery('.tabbedwidget').tabs();
    });


    <?php /*
    jQuery(function($){

        var editor = $('.summernote');
      editor.summernote({
        height: 200,
        focus: false,
        toolbar: [
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['font', ['strikethrough']],
          ['fontsize', ['fontsize']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['height', ['height']],
          ['view', ['fullscreen', 'codeview']],
        ],
        oninit: function() {
          // Add "open" - "save" buttons
          var noteBtn = '<button id="makeSnote" type="button" class="btn btn-default btn-sm btn-small" title="Add Code" data-event="something" tabindex="-1"><i class="fa fa-file-text"></i></button>';
          var fileGroup = '<div class="note-file btn-group">' + noteBtn + '</div>';
          $(fileGroup).appendTo($('.note-toolbar'));
          // Button tooltips
          $('#makeSnote').tooltip({
            container: 'body',
            placement: 'bottom'
          });
          // Button events
          $('#makeSnote').click(function(event) {
                    var range = window.getSelection().getRangeAt(0);
                    var node = $(range.commonAncestorContainer)
                    if (node.parent().is('code')) {
                        node.unwrap();
                    } else {
                        node = $('<code />')[0];
                        range.surroundContents(node);
                    };

          });
        },

      });

    });
    */ ?>
</script>

</body>
</html>
