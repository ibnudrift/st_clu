<?php
$this->breadcrumbs=array(
	'Perawat Category'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Perawat Category',
	'subtitle'=>'Edit Perawat Category',
);

$this->menu=array(
	array('label'=>'List Perawat Category', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Perawat Category', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Perawat Category', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>