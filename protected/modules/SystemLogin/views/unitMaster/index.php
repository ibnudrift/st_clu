<?php
$this->breadcrumbs=array(
	'Unit Masters',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Master Unit',
	'subtitle'=>'Data Master Unit',
);

$this->menu=array(
	// array('label'=>'Add Master Unit', 'icon'=>'plus-sign','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>
    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>
<?php endif; ?>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'unit-master-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'htmlOptions'=> array('class'=>'custom_table'),
	'columns'=>array(
		// 'id',
		// 'member_id',
		'ktp_no',
		'nama_pemilik',
		'project',
		'tipe_rumah',
		// 'blok',
		array(
			'header'=>'Blok Kav',
			'type'=>'raw',
			'value'=>'$data->blok ." ". $data->kav',
		),
		'phone',
		'bulan_st',
		// 'tahun_st',
		'bsc_marketing',
		'bsc_phone',
		/*
		'created_at',
		'updated_at',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			// &nbsp; {delete}
			'template'=>'{update}',
		),
	),
)); ?>
