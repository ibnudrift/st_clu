<?php
$this->breadcrumbs=array(
	'Tb Master Perawats'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List TbMasterPerawat','url'=>array('index')),
	array('label'=>'Add TbMasterPerawat','url'=>array('create')),
);
?>

<h1>Manage Tb Master Perawats</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'tb-master-perawat-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'nama',
		'email',
		'phone',
		'kelamin',
		'tempat_lahir',
		/*
		'tanggal_lahir',
		'tinggi_badan',
		'berat_badan',
		'anak_ke',
		'anak_dari',
		'agama',
		'riwayat_sakit',
		'loc_alamat_lengkap',
		'loc_provinsi',
		'loc_kabupaten',
		'loc_kecamatan',
		'loc_kota',
		'loc_kelurahan',
		'pendidikan_terakhir',
		'nama_sekolah',
		'lulus_tahun',
		'posisi_diminati',
		'keahlian',
		'pengalaman',
		'gaji_diinginkan',
		'nama_orangtua',
		'kerja_orangtua',
		'nama_suamiistri',
		'pekerjaan_suamiistri',
		'mengetahui_medisku',
		'foto_wajah',
		'foto_full',
		'berkas_lamaran',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
