<?php
$this->breadcrumbs=array(
	'Appointments'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Appointment','url'=>array('index')),
	array('label'=>'Add Appointment','url'=>array('create')),
);
?>

<h1>Manage Appointments</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'appointment-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'transaction_no',
		'book_datetime',
		'start_datetime',
		'end_datetime',
		'notes',
		/*
		'status',
		'unit_id',
		'user_id',
		'reschedules',
		'sts_ok',
		'prg_komplain',
		'created_at',
		'updated_at',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
