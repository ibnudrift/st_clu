<?php

/**
 * This is the model class for table "unit_master".
 *
 * The followings are the available columns in table 'unit_master':
 * @property string $id
 * @property integer $member_id
 * @property string $ktp_no
 * @property string $blok
 * @property string $kav
 * @property string $nama_pemilik
 * @property string $tipe_rumah
 * @property string $bsc_marketing
 * @property string $bulan_st
 * @property string $tahun_st
 * @property string $phone
 * @property string $project
 * @property string $created_at
 * @property string $updated_at
 */
class UnitMaster extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UnitMaster the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'unit_master';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('member_id', 'numerical', 'integerOnly'=>true),
			array('ktp_no, blok, kav, nama_pemilik, tipe_rumah', 'length', 'max'=>255),
			array('jadwal_st, phone, project', 'length', 'max'=>225),
			array('created_at, updated_at, unit_lunas, cluster_id, proyek_id, admin, bsc_phone, bsc_marketing, bulan_st, email', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, member_id, ktp_no, blok, kav, nama_pemilik, tipe_rumah, bsc_marketing, jadwal_st, phone, project, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'No.',
			'member_id' => 'Member',
			'ktp_no' => 'Ktp No',
			'blok' => 'Blok',
			'kav' => 'Kav',
			'nama_pemilik' => 'Nama Pemilik',
			'tipe_rumah' => 'Tipe Rumah',
			'bsc_marketing' => 'Bsc Marketing',
			'bsc_phone' => 'Bsc Phone',
			'jadwal_st' => 'Jadwal ST',
			'bulan_st' => 'Bulan ST',
			// 'tahun_st' => 'Tahun St',
			'phone' => 'Phone',
			'project' => 'Project',
			'admin' => 'Admin',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'unit_lunas' => 'Unit Lunas',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('member_id',$this->member_id);
		$criteria->compare('ktp_no',$this->ktp_no,true);
		$criteria->compare('blok',$this->blok,true);
		$criteria->compare('kav',$this->kav,true);
		$criteria->compare('nama_pemilik',$this->nama_pemilik,true);
		$criteria->compare('tipe_rumah',$this->tipe_rumah,true);
		$criteria->compare('bsc_marketing',$this->bsc_marketing,true);
		$criteria->compare('bsc_phone',$this->bsc_phone,true);
		$criteria->compare('jadwal_st',$this->jadwal_st,true);
		$criteria->compare('bulan_st',$this->bulan_st,true);
		$criteria->compare('admin',$this->admin,true);
		// $criteria->compare('tahun_st',$this->tahun_st,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('project',$this->project,true);
		$criteria->compare('unit_lunas',$this->unit_lunas,true);

		$criteria->order = 't.id DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=> false,
		));
	}
}