<?php
$this->breadcrumbs=array(
	'Clusters'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Cluster',
	'subtitle'=>'Add Cluster',
);

$this->menu=array(
	array('label'=>'List Cluster', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>