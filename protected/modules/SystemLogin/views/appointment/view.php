<?php
$this->breadcrumbs=array(
	'Appointments'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Appointment', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Appointment', 'icon'=>'plus-sign','url'=>array('create')),
	array('label'=>'Edit Appointment', 'icon'=>'pencil','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Appointment', 'icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1>View Appointment #<?php echo $model->id; ?></h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'transaction_no',
		'book_datetime',
		'start_datetime',
		'end_datetime',
		'notes',
		'status',
		'unit_id',
		'user_id',
		'reschedules',
		'sts_ok',
		'prg_komplain',
		'created_at',
		'updated_at',
	),
)); ?>
