<?php
$this->breadcrumbs=array(
	'Master Unit'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Master Unit',
	'subtitle'=>'Add Master Unit',
);

$this->menu=array(
	array('label'=>'List Master Unit', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>