<?php 
    $e_activemenu = $this->action->id;
    $controllers_ac = $this->id;
    $session=new CHttpSession;
    $session->open();
    $login_member = $session['login_member'];

    $active_menu_pg = $controllers_ac.'/'.$e_activemenu;
?>
<?php // echo ($active_menu_pg == 'home/index')? 'homes_pg':'inside_pg'; ?>


  <!-- Navigation -->
  <nav class="navbar navbar-light bg-light static-top">
    <div class="container">
      <a class="navbar-brand" href="#">
      	<img src="<?php echo $this->assetBaseurl; ?>logo-head.png" alt="" class="img img-fluid">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="list-inline pt-2">
          <?php if ($active_menu_pg != 'home/index'): ?>
          <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Back</a></li>  
          <?php else: ?>
        	<li class="list-inline-item"><a href="#" class="t-backtop">Home</a></li>
        	<li class="list-inline-item"><a href="#" class="toscroll" data-id="syarat-dv">Handover Requirements</a></li>
        	<li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/serahTerima')); ?>">Handover Registration</a></li>
        	<li class="list-inline-item"><a href="#" class="toscroll" data-id="contact-us">Contact Us</a></li>
          <?php endif ?>
        </ul>
      </div>

    </div>
  </nav>