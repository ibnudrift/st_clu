<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class JanjiForm extends CFormModel
{
	public $no_ktp,
			$proyek_id,
			$blok_tipe,
			$unit_name,
			$unit_id,
			$proyek_id,
			$transaction_no,
			$book_datetime,
			$start_datetime,
			$end_datetime,
			$data;
	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			array('no_ktp, proyek_id, blok_tipe, unit_id, transaaction_no', 'required', 'on'=>'insert'),
			// email has to be a valid email address
			// array('email', 'email'),
			array('no_ktp, proyek_id, blok_tipe, unit_name, unit_id, proyek_id, transaction_no, book_datetime, start_datetime, end_datetime, data', 'safe'),
			// verifyCode needs to be entered correctly
			// array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'no_ktp'=>Yii::t('main', 'No. KTP'),
			'proyek_id'=>Yii::t('main', 'Proyek'),
			'blok_tipe'=>Yii::t('main', 'Tipe Blok'),
			'unit_name'=>Yii::t('main', 'Nama Unit'),
			'unit_id'=>Yii::t('main', 'Unit'),
			'proyek_id'=>Yii::t('main', 'Proyek'),
			'transaction_no'=>Yii::t('main', 'Transaction No.'),
			'book_datetime'=>Yii::t('main', 'Book Date'),
			'start_datetime'=>Yii::t('main', 'Book TIme'),
			'end_datetime'=>Yii::t('main', 'End Date'),
			'data'=>Yii::t('main', 'Data'),
		);
	}
}