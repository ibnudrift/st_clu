<?php
$this->breadcrumbs=array(
	'Unit Masters'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List UnitMaster', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add UnitMaster', 'icon'=>'plus-sign','url'=>array('create')),
	array('label'=>'Edit UnitMaster', 'icon'=>'pencil','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete UnitMaster', 'icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1>View UnitMaster #<?php echo $model->id; ?></h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'member_id',
		'ktp_no',
		'blok',
		'kav',
		'nama_pemilik',
		'tipe_rumah',
		'jadwal_st_kontraktor',
		'jadwal_st_web',
		'bsc_marketing',
		'created_at',
		'updated_at',
	),
)); ?>
