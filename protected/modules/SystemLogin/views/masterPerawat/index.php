<?php
$this->breadcrumbs=array(
	'Master Perawat',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Master Perawat',
	'subtitle'=>'Data Master Perawat',
);

$this->menu=array(
	array('label'=>'Download Excel', 'icon'=>'download','url'=>array('download_excel')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'tb-master-perawat-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		'nama',
		'email',
		'phone',
		'kelamin',
		'pendidikan_terakhir',
		array(
			'header'=>'Provinsi',
			'type'=>'raw',
			'value'=>'City::model()->findByPk($data->loc_kota)->province',
		),
		array(
			'header'=>'Kota',
			'type'=>'raw',
			'value'=>'City::model()->findByPk($data->loc_kota)->city_name',
		),
		
		/*
		'loc_kecamatan',
		'tempat_lahir',
		'tanggal_lahir',
		'tinggi_badan',
		'berat_badan',
		'anak_ke',
		'anak_dari',
		'agama',
		'riwayat_sakit',
		'loc_alamat_lengkap',
		'loc_kabupaten',
		'loc_kelurahan',
		'nama_sekolah',
		'lulus_tahun',
		'posisi_diminati',
		'keahlian',
		'pengalaman',
		'gaji_diinginkan',
		'nama_orangtua',
		'kerja_orangtua',
		'nama_suamiistri',
		'pekerjaan_suamiistri',
		'mengetahui_medisku',
		'foto_wajah',
		'foto_full',
		'berkas_lamaran',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
		),
	),
)); ?>
