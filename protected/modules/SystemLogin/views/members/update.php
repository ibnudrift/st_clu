<?php
$this->breadcrumbs=array(
	'Member Appointment'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Member Appointment',
	'subtitle'=>'Edit Member Appointment',
);

$this->menu=array(
	array('label'=>'List Member Appointment', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Member Appointment', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Member Appointment', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>