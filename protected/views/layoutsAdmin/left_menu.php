<?php 
$session = new CHttpSession;
$session->open();
$checks_user = $session['login']['group_id'];
?>
<div class="leftmenu">        
    <ul class="nav nav-tabs nav-stacked">
        <li class="nav-header">Navigation</li>
        <?php if ($checks_user != 8): ?>

        <li class="dropdown"><a href="#"><span class="fa fa-folder"></span> <?php echo Tt::t('admin', 'Master Proyek') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/proyek')); ?>">Proyek</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/cluster')); ?>">Cluster</a></li>
            </ul>
        </li>

        <li class="dropdown"><a href="#"><span class="fa fa-folder"></span> <?php echo Tt::t('admin', 'Master Unit') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/unitMaster/index')); ?>">List Unit</a></li>                
                <!-- <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/unitMaster/csv')); ?>">Import CSV - Unit</a></li> -->
            </ul>
        </li>
        <li class="dropdown"><a href="#"><span class="fa fa-folder"></span> <?php echo Tt::t('admin', 'Appointments') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/appointment/index')); ?>">List Appointment</a></li>
            </ul>
        </li>
        <li>&nbsp;</li>
        <li class="dropdown"><a href="#"><span class="fa fa-folder"></span> <?php echo Tt::t('admin', 'List Admin BSC') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/administrator/index', 'type'=>'bsc')); ?>">List User Admin</a></li>
            </ul>
        </li>

        <li>&nbsp;</li>

        <li><a href="<?php echo CHtml::normalizeUrl(array('setting/index')); ?>"><span class="fa fa-cogs"></span> <?php echo Tt::t('admin', 'General Setting') ?></a>
        </li>
        <?php else: ?>
        
        <li class="dropdown"><a href="#"><span class="fa fa-folder"></span> <?php echo Tt::t('admin', 'Appointments') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/appointment/index')); ?>">List Appointment</a></li>
            </ul>
        </li>

        <li class="dropdown"><a href="#"><span class="fa fa-folder"></span> <?php echo Tt::t('admin', 'Master Unit') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/unitMaster/index')); ?>">List Unit</a></li>                
            </ul>
        </li>

        <?php endif ?>

        <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/home/logout')); ?>"><span class="fa fa fa-sign-out"></span> Logout</a></li>
    </ul>
</div><!--leftmenu-->
