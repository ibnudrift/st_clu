<?php

/**
 * This is the model class for table "tb_pesan_perawat".
 *
 * The followings are the available columns in table 'tb_pesan_perawat':
 * @property string $id
 * @property string $nama_lengkap
 * @property string $email
 * @property string $no_hp
 * @property string $provinsi
 * @property string $kabupaten
 * @property string $kecamatan
 * @property string $kelurahan
 * @property string $alamat_lengkap
 * @property string $nama_pasien
 * @property integer $usia_pasien
 * @property integer $tinggi_badan
 * @property integer $berat_badan
 * @property string $kota_tinggal_pasien
 * @property string $alat_yang_terpasang
 * @property string $luka_pada_pasien
 * @property string $kondisi_pasien
 * @property string $asisten_rumah_tangga
 * @property string $hewan_peliharaan
 */
class TbPesanPerawat extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TbPesanPerawat the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_pesan_perawat';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_lengkap', 'required'),
			array('usia_pasien, tinggi_badan, berat_badan', 'numerical', 'integerOnly'=>true),
			array('nama_lengkap, email, luka_pada_pasien, kondisi_pasien, asisten_rumah_tangga, hewan_peliharaan', 'length', 'max'=>225),
			array('no_hp, provinsi, kabupaten, kecamatan, kelurahan, alamat_lengkap, nama_pasien, kota_tinggal_pasien, alat_yang_terpasang', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nama_lengkap, email, no_hp, provinsi, kabupaten, kecamatan, kelurahan, alamat_lengkap, nama_pasien, usia_pasien, tinggi_badan, berat_badan, kota_tinggal_pasien, alat_yang_terpasang, luka_pada_pasien, kondisi_pasien, asisten_rumah_tangga, hewan_peliharaan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama_lengkap' => 'Nama Lengkap',
			'email' => 'Email',
			'no_hp' => 'No Hp',
			'provinsi' => 'Provinsi',
			'kabupaten' => 'Kabupaten',
			'kecamatan' => 'Kecamatan',
			'kelurahan' => 'Kelurahan',
			'alamat_lengkap' => 'Alamat Lengkap',
			'nama_pasien' => 'Nama Pasien',
			'usia_pasien' => 'Usia Pasien',
			'tinggi_badan' => 'Tinggi Badan',
			'berat_badan' => 'Berat Badan',
			'kota_tinggal_pasien' => 'Kota Tinggal Pasien',
			'alat_yang_terpasang' => 'Alat Yang Terpasang',
			'luka_pada_pasien' => 'Luka Pada Pasien',
			'kondisi_pasien' => 'Kondisi Pasien',
			'asisten_rumah_tangga' => 'Asisten Rumah Tangga',
			'hewan_peliharaan' => 'Hewan Peliharaan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('nama_lengkap',$this->nama_lengkap,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('no_hp',$this->no_hp,true);
		$criteria->compare('provinsi',$this->provinsi,true);
		$criteria->compare('kabupaten',$this->kabupaten,true);
		$criteria->compare('kecamatan',$this->kecamatan,true);
		$criteria->compare('kelurahan',$this->kelurahan,true);
		$criteria->compare('alamat_lengkap',$this->alamat_lengkap,true);
		$criteria->compare('nama_pasien',$this->nama_pasien,true);
		$criteria->compare('usia_pasien',$this->usia_pasien);
		$criteria->compare('tinggi_badan',$this->tinggi_badan);
		$criteria->compare('berat_badan',$this->berat_badan);
		$criteria->compare('kota_tinggal_pasien',$this->kota_tinggal_pasien,true);
		$criteria->compare('alat_yang_terpasang',$this->alat_yang_terpasang,true);
		$criteria->compare('luka_pada_pasien',$this->luka_pada_pasien,true);
		$criteria->compare('kondisi_pasien',$this->kondisi_pasien,true);
		$criteria->compare('asisten_rumah_tangga',$this->asisten_rumah_tangga,true);
		$criteria->compare('hewan_peliharaan',$this->hewan_peliharaan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}